<?php
/**
 * @file
 * Provides tokens for handling the pegasus event.
 *
 * @copyright Copyright(c) 2012 Previous Next Pty Ltd
 * @license GPL v2 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Lee Rowlands larowlan at previousnext dot com dot au
 */

/**
 * Implements hook_token_info().
 */
function pegasus_event_token_info() {
  $type = array(
    'name' => t('Pegasus event'),
    'description' => t('Tokens for pegasus events.'),
    'needs-data' => 'pegasus_event',
  );

  $tokens = array(
    'id' => array(
      'name' => t('Event ID'),
      'description' => t('ID for event.'),
    ),
    'action' => array(
      'name' => t('Event action'),
      'description' => t('Action that occured to trigger the event.'),
    ),
    'source_id' => array(
      'name' => t('Source ID'),
      'description' => t('Source ID for event.'),
    ),
    'site_id' => array(
      'name' => t('Source site id'),
      'description' => t('Source site id for event.'),
    ),
    'source_type' => array(
      'name' => t('Source type'),
      'description' => t('The type of source that triggered the event.'),
    ),
    'timestamp' => array(
      'name' => t('Event timestamp'),
      'description' => t('Timestamp `for event.'),
    ),
  );

  return array(
    'types' => array('pegasus_event' => $type),
    'tokens' => array('pegasus_event' => $tokens),
  );
}

/**
 * Implements hook_tokens().
 */
function pegasus_event_tokens($type, $tokens, $data = array(), $options = array()) {

  $replacements = array();
  if ($type == 'pegasus_event' && !empty($data['pegasus_event'])) {
    $properties = array(
      'uuid',
      'action',
      'sourceUuid',
      'sourceSiteId',
      'sourceType',
      'timestamp',
    );
    foreach ($properties as $property) {
      if (!empty($data['pegasus_event']->{$property}) && isset($tokens[$property])) {
        $replacements[$tokens[$property]] = check_plain($data['pegasus_event']->{$property});
      }
    }
  }

  return $replacements;
}
