# About Pegasus

The pegasus module provides a very basic API for handling CRUD requests on remote
content services. In order to use it, you must define your own server, extending
\PegasusClient() and implementing \PegasusClientInterface().

