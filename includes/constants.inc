<?php
/**
 * @file
 * Constant declarations for pegasus.
 *
 * @copyright Copyright(c) 2012 Previous Next Pty Ltd
 * @license GPL v2 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Chris Skene chris at previousnext dot com dot au
 */

/**
 * Define a default server class.
 *
 * @var string
 */
const PEGASUS_DEFAULT_SERVER_CLASS = 'Drupal\pegasus\Core\PegasusClient';

/**
 * Define the default server type.
 *
 * @var string
 */
const PEGASUS_DEFAULT_SERVER_TYPE = 'custom';

/**
 * Define a source type for an item with no source.
 *
 * This is used for creating dummy events for queue processing, where the event
 * itself has no explicit origin.
 *
 * @var string
 */
const PEGASUS_SOURCE_NONE = 'no_source';

/**
 * Define a queue state for a queue behaving normally.
 *
 * @var string
 */
const PEGASUS_QUEUE_STATE_NORMAL = 'normal';

/**
 * Define a queue state for a queue always loading all items.
 *
 * @var string
 */
const PEGASUS_QUEUE_STATE_CLEAN = 'clean';

/**
 * Define the maximum number of queue items to process in one go.
 *
 * @var int
 */
const PEGASUS_QUEUE_MAX_DEFAULT = 20;

/**
 * Define a constant for an item which has been queued.
 *
 * @var int
 */
const PEGASUS_QUEUE_QUEUE_SUCCESS = 1;

/**
 * Define a constant for an item which has failed queuing.
 *
 * @var int
 */
const PEGASUS_QUEUE_QUEUE_FAIL = 0;

/**
 * Define a constant for an item which already exists in a queue.
 *
 * @var int
 */
const PEGASUS_QUEUE_QUEUE_PASS = 2;

/**
 * Define a synchronisation type for items in a push state.
 */
const PEGASUS_SYNC_TYPE_PUSH = 'push';

/**
 * Define a synchronisation type for items in a pending push state.
 */
const PEGASUS_SYNC_TYPE_PENDING_PUSH = 'pending push';

/**
 * Define a synchronisation type for items in a pull state.
 */
const PEGASUS_SYNC_TYPE_PULL = 'pull';

/**
 * Define a synchronisation type for items in sync state.
 */
const PEGASUS_SYNC_TYPE_BOTH = 'two-way sync';

/**
 * Define a prefix for caching servers.
 */
const PEGASUS_SERVER_CACHE_PREFIX = 'pegasus_server_';

/**
 * Define a constant for items which should always log, regardless of debug.
 */
const PEGASUS_ALWAYS_LOG = 'always_log';
