<?php
/**
 * @file
 * Queue handling.
 *
 * @copyright Copyright(c) 2012 Previous Next Pty Ltd
 * @license GPL v2 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Chris Skene chris at previousnext dot com dot au
 */

/**
 * Wrapper to pass requests from an queue handler.
 *
 * @param \Drupal\pegasus\Event\EventInterface|object|array $event
 *   The queue item.
 */
function pegasus_queue_process_event($event) {

  // Convert the item.
  if (!$event instanceof \Drupal\pegasus\Event\EventInterface) {
    $event = pegasus_event_create($event);
  }

  // Load a server and run its queue processor.
  $server = pegasus_get_client($event->getServer());

  // Implement state switching.
  $server->alterEvent($event);

  // Invoke all event processing.
  // Invokes 'hook_pegasus_process_event'
  module_invoke_all('pegasus_process_event', $event);

  // Invoke server specific hook processing.
  // Invokes 'hook_event_process_event_SERVER-NAME'
  module_invoke_all('pegasus_process_event_' . $event->getServer(), $event);

  // Check for errors.
  $errors = $event->getErrors();
  if (!empty($errors)) {
    drupal_set_message(t('An error occurred whilst handling the %method method for an item, the item was re-queued for later processing. Please refer to logs for more details.',
      array('%method' => $event->getAction())
    ));

    // If debugging and devel are present, dump like the wind.
    if (pegasus_debug_is_on() && function_exists('dpm')) {
      dpm($errors);
    }
  }

  // Log it.
  watchdog('pegasus', 'Processed queued event %id for server %name', array('%id' => $event->getId(), '%name' => $event->getServer()), WATCHDOG_NOTICE);
}

/**
 * Load a fully populated queue handler.
 *
 * For a fully populated queue handler, both parameters must be provided.
 *
 * @param string $server_name
 *   (optional) The server name.
 * @param string $action
 *   (optional) An action to get a queue for.
 *
 * @return Drupal\pegasus\Queue\QueueHandler|Drupal\pegasus\Queue\EventQueueHandler|bool
 *   A queue handler.
 */
function pegasus_queue_handler($server_name = NULL, $action = NULL) {
  $queue_handler = FALSE;

  if (empty($server_name)) {
    $queue_handler = new \Drupal\pegasus\Queue\QueueHandler();
  }
  elseif (!empty($action)) {
    $server = pegasus_get_client($server_name);
    $queue_handler = $server->getQueueHandler($action);
  }

  return $queue_handler;
}

/**
 * Wrapper to load a queue from a queue name.
 *
 * @param string $queue_name
 *   The queue name.
 *
 * @return bool|Drupal\pegasus\Queue\EventQueueHandler|Drupal\pegasus\Queue\QueueHandler
 *   The queue.
 */
function pegasus_queue_handler_from_queue_name($queue_name) {
  $queues = pegasus_queue_get_queues();
  if (array_key_exists($queue_name, $queues)) {
    $handler = pegasus_queue_handler($queues[$queue_name]['server'], $queues[$queue_name]['action']);
    return $handler;
  }

  return FALSE;
}

/**
 * Create a QueueHandler.
 *
 * @param array|string $action
 *   (optional) An action configuration from getActions(), or an action name.
 *
 * @return \Drupal\pegasus\Queue\QueueInterface
 *   A Queue Handler.
 */
function pegasus_queue_handler_create($action) {

  // Load the queue handler.
  if (is_array($action) && isset($action['class'])) {
    $class = $action['class'];
  }
  else {
    $class = '\Drupal\pegasus\Queue\QueueHandler';
  }
  return new $class();
}

/**
 * Get all defined queues for all servers.
 *
 * @return array
 *   An array of queues.
 */
function pegasus_queue_get_queues() {
  $queues = &drupal_static(__FUNCTION__);
  if (!isset($queues)) {
    $servers = pegasus_get_server_configurations();

    if (!empty($servers)) {
      foreach ($servers as $server_name => $server_config) {
        $server = pegasus_get_client($server_name);

        if (!is_object($server)) {
          continue;
        }

        foreach ($server->getQueues() as $id => $queue) {
          $queue_obj = pegasus_queue_load($id);
          $queue['server'] = $server_name;
          $queue['items'] = 0;
          if ($queue_obj) {
            $queue['items'] = $queue_obj->unclaimedItems();
            $queue['claimed'] = $queue_obj->claimedItems();
          }
          $queues[$id] = $queue;
        }
      }
    }
  }

  return $queues;
}

/**
 * Get the maximum number of items to process in a queue.
 *
 * @param string $queue_name
 *   (optional) A queue name.
 *
 * @return int
 *   The number of items to process.
 */
function pegasus_queue_get_max_events($queue_name = NULL) {
  if (isset($queue_name) && !empty($queue_name)) {
    return variable_get('pegasus_queue_max_' . $queue_name, PEGASUS_QUEUE_MAX_DEFAULT);
  }

  return variable_get('pegasus_queue_max', PEGASUS_QUEUE_MAX_DEFAULT);
}

/**
 * Helper to load a queue.
 *
 * @param string $name
 *   The name of the queue to load.
 *
 * @return Drupal\pegasus\Queue\PegasusQueue
 *   A queue object.
 */
function pegasus_queue_load($name) {
  return new \Drupal\pegasus\Queue\PegasusQueue($name);
}

/**
 * Execute a queue immediately using a batch process.
 *
 * @param string $queue_name
 *   The Queue identifier.
 */
function pegasus_queue_execute($queue_name) {
  $defined_queues = pegasus_queue_get_queues();
  $info = $defined_queues[$queue_name];
  $queue = pegasus_queue_load($queue_name);

  if ($queue->numberOfItems() > 0) {
    $batch = array(
      'title' => t('Processing @title', array('@title' => $info['title'])),
      'operations' => array(
        array('pegasus_queue_batch_handler', array($queue, $info)),
      ),
      'finished' => 'pegasus_queue_batch_finished',
    );

    batch_set($batch);
  }
}