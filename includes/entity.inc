<?php
/**
 * @file
 * Entity-related functions for pegasus.
 *
 * @copyright Copyright(c) 2012 Previous Next Pty Ltd
 * @license GPL v2 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Chris Skene chris at previousnext dot com dot au
 */

/**
 * Get information about entity handlers.
 *
 * Invokes 'hook_pegasus_entity_handlers'
 *
 * @return array
 *   An array of entity handler information.
 */
function pegasus_entity_handler_info() {
  $handlers = &drupal_static(__FUNCTION__);

  if (!isset($handlers)) {
    $handlers = module_invoke_all('pegasus_entity_handlers');
  }

  return $handlers;
}

/**
 * Call all sync implementations and let them act on the entity change.
 *
 * This calls the relevant handler given for a sync implementation,
 * passing it the change type, entity and entity type. It is then up to the
 * handler to decide whether to queue, push, pull etc.
 *
 * This is called by hook_entity_insert(), hook_entity_update() and
 * hook_entity_delete(), for the pegasus module.
 *
 * @param string $change_type
 *   By default, 'insert', 'update' and 'delete', however others are
 *   theoretically possible.
 * @param string $entity_type
 *   The entity type.
 * @param string $entity
 *   The entity.
 */
function pegasus_entity_process_change($change_type, $entity_type, $entity) {
  $sync_implementations = pegasus_sync_type_info();

  if (!empty($sync_implementations)) {
    foreach ($sync_implementations as $name => $implementation) {
      if (array_key_exists('entity process callback', $implementation) &&
        function_exists($implementation['entity process callback'])) {

        call_user_func($implementation['entity process callback'], $entity, $change_type, $entity_type);
      }
    }
  }
}