<?php
/**
 * @file
 * Internal functions for pegasus.
 *
 * @copyright Copyright(c) 2012 Previous Next Pty Ltd
 * @license GPL v2 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Chris Skene chris at previousnext dot com dot au
 */

/**
 * Invoke an action for a given handler.
 *
 * @param string $action
 *   The action to invoke (e.g. 'requeue')
 * @param string $handler
 *   The sync handler.
 * @param string $server
 *   The server being accessed.
 * @param string $entity_type
 *   The entity type.
 * @param object $entity
 *   The entity.
 * @param array $data
 *   Any other data which might be expected by the handler. Note that if any of
 *   the keys above are set, they will overwrite the same key in the data array.
 */
function pegasus_invoke_admin_action($action, $handler, $server, $entity_type, $entity, $data = array()) {
  // Load required sync info.
  $sync_implementations = pegasus_sync_type_info();
  $bundle = pegasus_get_entity_bundle($entity_type, $entity);
  $sync_status = pegasus_entity_sync_status($entity_type, $bundle, $entity->uuid);

  if (array_key_exists('operations', $sync_implementations[$handler])) {
    $operations = $sync_implementations[$handler]['operations'];
    if (array_key_exists($action, $operations)) {
      if (array_key_exists('callback', $operations[$action]) && function_exists($operations[$action]['callback'])) {
        $data['entity_type'] = $entity_type;
        $data['entity'] = $entity;
        $data['bundle'] = $bundle;
        $data['action'] = $action;
        $data['source'] = $server;
        $data['handler'] = $handler;
        $data['sync_status'] = $sync_status;

        call_user_func($operations[$action]['callback'], $data);
      }
    }
  }
}

/**
 * Helper to get a value from an object or array.
 *
 * @param array|object $item
 *   An item to get properties from
 * @param string $key
 *   Key of the item to return
 *
 * @return mixed
 *   The value of the item, or FALSE.
 */
function pegasus_set_property($item, $key) {
  if (is_object($item)) {
    $item = (array) $item;
  }

  if (array_key_exists($key, $item)) {
    return $item[$key];
  }

  return FALSE;
}

/**
 * Helper to get an entity bundle key.
 *
 * @param string $entity_type
 *   An entity type.
 *
 * @return string
 *   A bundle key.
 */
function pegasus_get_entity_bundle_key($entity_type) {

  $info = entity_get_info($entity_type);

  if (!empty($info['entity keys']['bundle'])) {

    return $info['entity keys']['bundle'];
  }

  return 'type';
}

/**
 * Helper to get an entity bundle.
 *
 * @param string $entity_type
 *   An entity type.
 * @param object $entity
 *   An entity.
 *
 * @return string
 *   The bundle.
 */
function pegasus_get_entity_bundle($entity_type, $entity) {
  $key = pegasus_get_entity_bundle_key($entity_type);
  return $entity->{$key};
}

/**
 * Load an array of Drupal entity wrappers given a source_id.
 *
 * @param string $source_id
 *   The source id of the item.
 *
 * @return array
 *   Array of entity metadata wrappers.
 */
function pegasus_load_entities_by_source($source_id) {
  $entities = drupal_static(__FUNCTION__, array());
  // We statically cache by source_id.
  if (empty($entities[$source_id])) {
    $wrappers = module_invoke_all('pegasus_entities_by_source', $source_id);
    $entities[$source_id] = $wrappers;
  }
  return $entities[$source_id];
}