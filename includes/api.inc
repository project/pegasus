<?php
/**
 * @file
 * Main API for pegasus.
 *
 * @copyright Copyright(c) 2012 Previous Next Pty Ltd
 * @license GPL v2 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Chris Skene chris at previousnext dot com dot au
 */

/**
 * Load server types.
 *
 * Server manager can only provide servers if it knows the server types
 * available, as each server type must provide a PegasusClient implementation.
 *
 * @todo: caching
 *
 * @return array
 *   An array of server type information.
 */
function pegasus_get_server_types() {
  $types = module_invoke_all('pegasus_server_types');
  return $types;
}

/**
 * Get server implementations.
 *
 * Server implementations are classes which implement PegasusInterface(),
 * and are declared using hook_pegasus_server().
 *
 * @param bool $flush
 *   Whether to flush the cache or not.
 *
 * @return array
 *   An array of server information.
 */
function pegasus_get_server_configurations($flush = FALSE) {
  static $servers;
  static $cached;

  if (!isset($cached) || $cached != TRUE || $flush == TRUE) {
    // Invokes hook_pegasus_server
    $servers = module_invoke_all('pegasus_server_servers');
    // Invokes hook_pegasus_server_types();
    $types = pegasus_get_server_types();

    foreach ($servers as $machine_name => $config) {

      // Set basic server properties.
      $servers[$machine_name] = array(
        'name' => $machine_name,
        'title' => $config['title'],
        'description' => $config['description'],
        'disabled' => FALSE,
        'type' => PEGASUS_DEFAULT_SERVER_TYPE,
      );

      // The server key contains server-specific settings.
      isset($config['settings']) ? $servers[$machine_name]['settings'] = $config['settings'] : $servers[$machine_name]['settings'] = NULL;

      // If a type is provided, us it.
      if (isset($config['type'])) {
        $servers[$machine_name]['type'] = $config['type'];
      }

      // If the server provides its own class, use it.
      if (isset($config['class']) && !empty($config['class'])) {
        $servers[$machine_name]['class'] = $config['class'];
      }
      // Otherwise, use one from the server_type, if available.
      elseif (isset($config['type']) && array_key_exists($config['type'], $types)) {
        $servers[$machine_name]['class'] = $types[$config['type']]['class'];
      }
      // If none is found, disable the server to stop things breaking.
      else {
        $servers[$machine_name]['disabled'] = TRUE;
      }
    }
    $cached = TRUE;
  }

  // Invoke 'hook_pegasus_servers_alter'.
  drupal_alter('pegasus_servers', $servers);

  return $servers;
}

/**
 * Get an individual server implementation.
 *
 * @param string $name
 *   Name of the server to return
 *
 * @return array|bool
 *   A pegasus server configuration, or FALSE if none was found.
 */
function pegasus_get_server_configuration($name) {
  $servers = pegasus_get_server_configurations();

  if (isset($servers[$name])) {
    return $servers[$name];
  }

  return FALSE;
}

/**
 * Load a server object for use.
 *
 * Loaded server objects are cached statically for performance.
 *
 * @param string $server_name
 *   Name of the server configuration to use, as defined in hook_pegasus_server().
 * @param bool $flush
 *   (optional) If TRUE, flush the static servers cache. Defaults to FALSE.
 *
 * @throws Exception
 *
 * @return \Drupal\pegasus\Core\PegasusClientInterface
 *   The server object to use.
 */
function pegasus_get_client($server_name, $flush = FALSE) {
  $loaded_servers = drupal_static('pegasus_get_client', NULL, $flush);

  if (isset($loaded_servers[$server_name])) {
    return $loaded_servers[$server_name];
  }

  $cached_server = cache_get(PEGASUS_SERVER_CACHE_PREFIX . $server_name);
  if (!empty($cached_server) && $cached_server instanceof \Drupal\pegasus\Core\PegasusInterface) {
    $loaded_servers[$server_name] = $cached_server;
    return $cached_server;
  }

  $servers = pegasus_get_server_configurations($flush);

  // We have a server configuration.
  if (isset($servers[$server_name])) {

    // Ctools (and some modules) may disable the server.
    if (isset($servers[$server_name]['disabled']) && $servers[$server_name]['disabled'] == TRUE) {
      watchdog('pegasus', 'The requested Pegasus server %server is disabled and won\'t be loaded', array('%server' => $server_name));
      return FALSE;
    }

    // Load a server class.
    isset($servers[$server_name]['class']) ? $class = $servers[$server_name]['class'] : $class = PEGASUS_DEFAULT_SERVER_CLASS;
    if (class_exists($class)) {
      $loaded_servers[$server_name] = new $class($server_name, $servers[$server_name]);
      $server = $loaded_servers[$server_name];
      cache_set(PEGASUS_SERVER_CACHE_PREFIX . $server_name, $server);
      return $server;
    }
    else {
      throw new Exception('Invalid server class: ' . $servers[$server_name]['class'] . ' specified for ' . $server_name);
    }
  }
  return FALSE;
}

/**
 * Get the human readable name of a server configuration.
 *
 * @param string $name
 *   The name of the server.
 *
 * @return string|bool
 *   The human readable name, or FALSE.
 */
function pegasus_get_client_title($name) {
  $servers = pegasus_get_server_configurations();
  if (isset($servers[$name])) {
    return $servers['title'];
  }
  return FALSE;
}

/**
 * Enable a server.
 *
 * @param string $name
 *   Name of the server to enable.
 */
function pegasus_flush_client_settings($name) {

  $server = pegasus_get_client($name, TRUE);


  $queues = $server->getQueues();

  if (!empty($queues)) {
    foreach ($queues as $queue_name => $queue_config) {
      variable_set('queue_class_' . $queue_name, '\Drupal\pegasus\Queue\PegasusQueue');
    }
  }
}

/**
 * Given an entity, determine its sync status.
 *
 * @param string $entity_type
 *   An entity type.
 * @param string $bundle
 *   An entity bundle
 * @param string $id
 *   The ID.
 *
 * @return \Drupal\pegasus\Core\SyncStatus
 *   A sync tracker.
 */
function pegasus_entity_sync_status($entity_type, $bundle, $id) {
  $pegasus_implementations = pegasus_sync_type_info();
  $sync_status = pegasus_sync_tracker_create($id);

  if (!empty($pegasus_implementations)) {
    foreach ($pegasus_implementations as $name => $settings) {
      if (isset($settings['status callback']) && function_exists($settings['status callback'])) {
        $status = call_user_func($settings['status callback'], $entity_type, $bundle, $id);
        $sync_status->mergeSyncTracker($status);
      }
    }
  }

  return $sync_status;
}

/**
 * Given a server, collection and identifier, import and save all entities.
 *
 * Call this if you want to import an entity and match against any existing
 * items.
 *
 * @todo: currently only matches 1 entity.
 *
 * @param string $server
 *   Name of the server to use.
 * @param string $collection
 *   The collection to access. This is usually the same as the entity_type.
 * @param string $id
 *   The identifier of the remote object to import.
 *
 * @return array
 *   An array of results.
 */
function pegasus_import_entities($server, $collection, $id) {
  $results[] = array();

  $server = pegasus_get_client($server);

  if (!empty($server)) {
    $matched_entities[] = $server->matchEntity($collection, $id);

    if (!empty($matched_entities)) {
      foreach ($matched_entities as $matched_entity) {
        $results[] = pegasus_import_entity($server, $collection, $id, $matched_entity);
      }
    }
    else {
      $results[] = pegasus_import_entity($server, $collection, $id);
    }
  }

  return $results;
}

/**
 * Helper to import an entity.
 *
 * Call this if you just want an import. It has parameters to accommodate entity
 * matching, however its easier to call pegasus_import_entities() for that.
 *
 * @param string|\Drupal\pegasus\Core\PegasusInterface $server
 *   Name of the server to use, or a server object.
 * @param string $collection
 *   The collection to access. This is usually the same as the entity_type (and
 *   is currently broken if its different).
 * @param string $id
 *   The identifier of the remote object to import.
 * @param object $matched_entity
 *   (optional) An entity to match against.
 * @param bool $mock
 *   (optional) If true, the entity is not saved.
 *
 * @return array
 *   The result for this request.
 */
function pegasus_import_entity($server, $collection, $id, $matched_entity = NULL, $mock = FALSE) {

  // Load a pegasus server if we haven't got one.
  if (!is_object($server)) {
    $server = pegasus_get_client($server);
  }

  if (!empty($server)) {

    $event = pegasus_event_create_default();
    $event->setSourceId($id);
    $event->setAction('pull');
    $event->setSourceType($collection);
    $event->setServer($server->getName());

    if (!empty($matched_entity)) {
      $event->setData('matched_entity', $matched_entity);
    }

    global $user;
    $event->setRequestingUser($user->uid);

    // Use the import() wrapper, to make it a bit easier.
    return $server->import($event, $mock);
  }
  else {
    watchdog('pegasus', 'Could not load a server for entity import', WATCHDOG_ERROR);
  }

  return FALSE;
}

/**
 * Get information about sync implementations.
 *
 * Invokes 'hook_pegasus_sync_types'
 *
 * @return array
 *   An array of entity handler information.
 */
function pegasus_sync_type_info() {
  $handlers = &drupal_static(__FUNCTION__);

  if (!isset($handlers)) {
    $handlers = module_invoke_all('pegasus_sync_types');
  }

  return $handlers;
}

/**
 * Determine if a sync type is provided for a given entity type.
 *
 * @param string $entity_type
 *   The entity type.
 *
 * @return bool
 *   TRUE, if the type has a provided sync handler.s
 */
function pegasus_entity_has_sync_type($entity_type) {
  $types = pegasus_sync_type_info();
  foreach ($types as $key => $type) {
    if (isset($type['types'])) {
      if (array_key_exists($entity_type, $type['types'])) {
        return TRUE;
      }
    }
  }

  return FALSE;
}

/**
 * Get the default user for new content.
 *
 * This value is used when now local user can be found for a content item
 * being imported.
 *
 * @param string|null $server_name
 *   (optional) The server name.
 *
 * @return int/string
 *   The UID of the default user.
 */
function pegasus_get_default_user($server_name = NULL) {
  if (!empty($server_name)) {
    $server_user = variable_get('pegasus_event_default_user_' . $server_name, FALSE);
    if (isset($server_user) && !empty($server_user)) {
      return $server_user;
    }
  }

  return variable_get('pegasus_event_default_user', 1);
}

/**
 * Set the default user uid.
 *
 * @param string|int $uid
 *   The uid of the user to set.
 * @param string|null $server_name
 *   (optional) The server name. If not provided, the default is set.
 *
 * @return bool
 *   TRUE on success, or FALSE.
 */
function pegasus_set_default_user($uid, $server_name = NULL) {
  $user = user_load($uid);

  if (empty($user)) {
    drupal_set_message('Invalid user provided for default user assignment', 'error');
    return FALSE;
  }

  if (isset($server_name) && !empty($server_name)) {
    variable_set('pegasus_event_default_user_' . $server_name, $uid);
  }
  else {
    variable_get('pegasus_event_default_user', $uid);
  }

  return TRUE;
}

/**
 * Create a new sync tracker.
 *
 * Functions providing information on an items status should return a sync
 * tracker object with all the relevant properties set.
 *
 * @param string|null $id
 *   The ID of the local item to track.
 *
 * @return Drupal\pegasus\Core\SyncStatus
 *   A sync tracker object.
 */
function pegasus_sync_tracker_create($id = NULL) {
  return new \Drupal\pegasus\Core\SyncStatus($id);
}

/**
 * Create an Event.
 *
 * @param array $item
 *   An item, as returned by the queue interface.
 *
 * @return \Drupal\pegasus\Event\Event
 *   An event item.
 */
function pegasus_event_create($item) {

  // Create an Event item, and set its properties.
  $event = new \Drupal\pegasus\Event\Event();

  $event->setId(pegasus_set_property($item, 'id'));
  $event->setSiteId(pegasus_set_property($item, 'site_id'));
  $event->setSourceId(pegasus_set_property($item, 'source_id'));
  $event->setSourceType(pegasus_set_property($item, 'source_type'));
  $event->setSourceSubtype(pegasus_set_property($item, 'source_subtype'));
  $event->setTimestamp(pegasus_set_property($item, 'timestamp'));
  $event->setState(pegasus_set_property($item, 'state'));
  $event->setAction(pegasus_set_property($item, 'action'));
  $event->setServer(pegasus_set_property($item, 'server'));
  $event->setQueuedTimestamp(REQUEST_TIME);

  return $event;
}

/**
 * Helper to create a new, queueable event.
 *
 * @return \Drupal\pegasus\Event\Event
 *   An event with some default properties set.
 */
function pegasus_event_create_default() {

  // Create an Event item, and set its properties.
  $event = new \Drupal\pegasus\Event\Event();
  $event->setTimestamp(time());
  $event->setId(uuid_generate());

  return $event;
}

/**
 * Fetch and populate the event queue for a server.
 *
 * @param string $server_name
 *   The server name.
 */
function pegasus_events_fetch($server_name) {

  // Init our queue.
  $queue = pegasus_queue_handler($server_name, 'events');
  $queue->loadQueue('events');

  // Get our remote events for the current server.
  $events = $queue->getEvents();

  if (!empty($events)) {

    $queue->populateQueue($events);
  }
}

/**
 * Create a save result object.
 *
 * @return \Drupal\pegasus\Core\SaveResult
 *   A new SaveResult object.
 */
function pegasus_save_result_create() {
  return new \Drupal\pegasus\Core\SaveResult();
}