<?php
/**
 * @file
 * Debugging functionality for pegasus
 *
 * @copyright Copyright(c) 2012 Previous Next Pty Ltd
 * @license GPL v2 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Chris Skene chris at previousnext dot com dot au
 */

/**
 * Get the current debugging state.
 */
function pegasus_debug_is_on() {
  if (variable_get('pegasus_debugging', 'off') == 'on') {
    return TRUE;
  }

  return FALSE;
}

/**
 * Log error messages.
 *
 * Since this passes straight on to watchdog(), all the values passed should
 * match those expected by that function.
 *
 * @param string $message
 *   The message.
 * @param array $substitutions
 *   (optional) An array of substitutions for the message.
 * @param int|string $type
 *   (optional) The type. Use watchdog() constants. Defaults to
 *   WATCHDOG_ERROR.
 * @param bool $repeat
 *   (optional) If FALSE, do not repeat identical messages in drupal_set_message
 * @param bool|string $always_log
 *   (optional) If set to PEGASUS_ALWAYS_LOG, this will ignore debug settings.
 */
function pegasus_log($message, $substitutions = array(), $type = WATCHDOG_ERROR, $repeat = TRUE, $always_log = FALSE) {

  // Mark this page as being uncacheable.
  drupal_page_is_cacheable(FALSE);

  // Log to watchdog.
  watchdog('pegasus', $message, $substitutions, $type);

  // Pass the error to the debug function as well, in case that is on and we
  // want output.
  $m = format_string($message, $substitutions);
  pegasus_debug($m, $type, $repeat, $always_log);
}

/**
 * Log debug messages.
 *
 * @param string $message
 *   The message.
 * @param string $type
 *   (optional) The type. Use drupal_set_message types.
 * @param bool $repeat
 *   (optional) If FALSE, the message will not be repeated.
 * @param bool|string $always_log
 *   (optional) If set to PEGASUS_ALWAYS_LOG, this will ignore debug settings.
 */
function pegasus_debug($message, $type = 'status', $repeat = TRUE, $always_log = FALSE) {

  // If debugging is on, log to the screen.
  if (pegasus_debug_is_on() || $always_log == PEGASUS_ALWAYS_LOG) {

    // Log messages sent from PegasusClient::logError will have a watchdog
    // constant set, so translate that to an appropriate message type.
    $watchdog_error_types = array(
      WATCHDOG_INFO => 'status',
      WATCHDOG_NOTICE => 'status',
      WATCHDOG_DEBUG => 'status',
      WATCHDOG_WARNING => 'warning',
      WATCHDOG_ALERT => 'error',
      WATCHDOG_ERROR => 'error',
      WATCHDOG_CRITICAL => 'error',
      WATCHDOG_EMERGENCY => 'error',
    );
    if (array_key_exists($type, $watchdog_error_types)) {
      $type = $watchdog_error_types[$type];
    }

    drupal_set_message($message, $type, $repeat);
  }
}

/**
 * Helper to dump arrays/objects to the watchdog.
 *
 * @param mixed $item
 *   The item to dump.
 * @param string $message
 *   (Optional) An optional message.
 */
function pegasus_debug_dump($item, $message = 'Dumped result: ') {
  if (pegasus_debug_is_on()) {
    watchdog('pegasus', '%message: <pre>@arguments</pre>', array(
      '%message' => $message,
      '@arguments' => print_r($item, TRUE),
    ), WATCHDOG_DEBUG);
  }
}

