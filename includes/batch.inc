<?php
/**
 * @file
 * Batch functionality for pegasus.
 *
 * @copyright Copyright(c) 2012 Previous Next Pty Ltd
 * @license GPL v2 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Chris Skene chris at previousnext dot com dot au
 */

/**
 * Batch api callback for processing pegasus_queue items.
 *
 * @param \Drupal\pegasus\Queue\PegasusQueue $queue
 *   A Queue object.
 * @param array $info
 *   Batch handler info.
 * @param array $context
 *   Batch handler context.
 */
function pegasus_queue_batch_handler($queue, $info, &$context) {
  $max_queue_items = pegasus_queue_get_max_events($queue->name());

  if (empty($context['sandbox'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['current'] = 0;
    $context['sandbox']['max'] = $queue->numberOfItems();
  }

  for ($i = 0; $i < $max_queue_items && $context['sandbox']['current'] < $context['sandbox']['max']; $i++) {
    $item = $queue->claimItem();
    if ($item) {
      // Do the processing.
      $function = $info['worker callback'];
      try {
        $result = $function($item->data);
        $queue->deleteItem($item);
        $context['results'][] = array('item' => $item, 'result' => $result);
      }
      catch(\Exception $e) {
        pegasus_log($e->getMessage(), array());
      }
    }
    $context['sandbox']['progress']++;
    $context['sandbox']['current']++;
  }
  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }
}

/**
 * Batch api finished callback.
 */
function pegasus_queue_batch_finished($success, $results, $operations) {
  if ($success) {
    $message = format_plural(count($results), '1 item processed.', '@count items processed.');
  }
  else {
    $message = t('Finished with an error.');
  }
  drupal_set_message($message);
}
