<?php
/**
 * @file
 * Page callbacks for the pegasus_admin modules server feature.
 *
 * @license GPL v2 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Chris Skene chris at previousnext dot com dot au
 */

/**
 * Page callback for the overview page.
 *
 * @return string
 *   The output of the overview page.
 */
function pegasus_admin_servers_page() {
  $output = '<h2>Available servers</h2>';

  $servers = pegasus_get_server_configurations();

  if (!empty($servers)) {
    $output .= pegasus_admin_render_server_table($servers, TRUE, TRUE);
  }
  else {
    $output .= '<p><em>No servers available</em></p>';
  }

  return $output;
}

/**
 * Render a server table.
 *
 * @param array $servers
 *   An array of server information as provided by
 *   pegasus_get_server_configurations().
 * @param bool $show_links
 *   Whether to show a link to the servers details page.
 * @param bool $show_types
 *   Whether to append a list of server types below the table. Defaults to FALSE
 *
 * @return string
 *   Output for rendering.
 */
function pegasus_admin_render_server_table($servers, $show_links = TRUE, $show_types = FALSE) {
  if (empty($servers)) {
    return '';
  }

  // Table header.
  $header = array(
    t('Name'),
    t('Type'),
    t('Server'),
    t('Description'),
    t('Status'),
    t('Operations'),
  );
  $rows = array();
  $used_types = array();

  $types = pegasus_get_server_types();

  foreach ($servers as $server_name => $server) {

    // Determine a type.
    if (isset($server['type'])) {
      if ($server['type'] == PEGASUS_DEFAULT_SERVER_TYPE) {
        $type = t('Custom');
        $used_types['custom'] = t('Custom: A custom server type.');
      }
      elseif (array_key_exists($server['type'], $types)) {
        $type = $server['type'];
        $used_types[$server['type']] = $types[$server['type']]['title'] . ': ' . $types[$server['type']]['description'] . '';
      }
      else {
        $type = $server['type'] . t(' (unknown/missing server type)');
        $used_types[$server['type']] = $server['type'] . t(': unknown/missing server type');
      }
    }
    else {
      $type = t('Unknown');
    }

    // Get URL.
    if (isset($server['settings']) && isset($server['settings']['base_url'])) {
      $base_url = l($server['settings']['base_url'], $server['settings']['base_url']);
    }
    else {
      $base_url = '<em>' . t('Not specified') . '</em>';
    }

    // Get status.
    isset($server['disabled']) && $server['disabled'] == TRUE ? $disabled = '<em>disabled</em>' : $disabled = 'enabled';

    // Build a path.
    $name_path = str_replace('_', '-', $server_name);

    // Row information.
    $row = array(
      'title' => array(
        'data' => $server['title'],
      ),
      'type' => array(
        'data' => $type,
      ),
      'url' => array(
        'data' => $base_url,
      ),
      'description' => array(
        'data' => $server['description'],
      ),
      'status' => array(
        'data' => $disabled,
      ),
    );

    $operations = array(
      'details' => l(t('Details'), PEGASUS_ADMIN_BASE_PATH . '/servers/' . $name_path),
      'flush' => l(t('Flush'), PEGASUS_ADMIN_BASE_PATH . '/servers/' . $name_path . '/flush'),
      'fetch_events' => l(t('Fetch events'), PEGASUS_ADMIN_BASE_PATH . '/servers/' . $name_path . '/fetch-events'),
    );

    if ($show_links == TRUE) {
      $row['operations'] = array(
        'data' => implode(' | ', $operations),
      );
    }
    else {
      unset($operations['details']);
      $row['operations'] = array(
        'data' => implode(' | ', $operations),
      );
    }

    $rows[$server_name] = $row;
  }

  $vars = array(
    'header' => $header,
    'rows' => $rows,
  );
  $output = theme('table', $vars);

  if (!empty($used_types) && $show_types == TRUE) {
    $output .= '<h3>' . t('Server types') . '</h3>' . "\n";
    foreach ($used_types as $type) {
      $output .= $type;
    }
  }

  return $output;
}
/**
 * Page callback: Administration page for an individual server.
 *
 * @param array $server_config
 *   An array of server configuration information.
 *
 * @return string
 *   The rendered output of the page.
 *
 * @see pegasus_admin_menu()
 */
function pegasus_admin_server_admin($server_config) {
  $output = '';

  // Server details.
  $output .= drupal_render(drupal_get_form('pegasus_admin_server_details'));

  // Options.
  $output .= drupal_render(drupal_get_form('pegasus_admin_server_events_form'));

  // Test pull.
  $output .= drupal_render(drupal_get_form('pegasus_admin_server_test_pull_form'));

  // Show the testing form if the server supports it.
  $server = pegasus_get_client($server_config['name']);
  if (method_exists($server, 'testConnection')) {
    $output .= drupal_render(drupal_get_form('pegasus_admin_server_test_connection_form'));
  }

  return $output;
}

/**
 * Form constructor to provide form details.
 *
 * @ingroup forms
 */
function pegasus_admin_server_details($form, $form_state) {

  $server_name = str_replace('-', '_', check_plain(arg(5)));

  $server_config = pegasus_get_server_configuration($server_name);

  $form['details'] = array(
    '#type' => 'fieldset',
    '#title' => t('Information'),
  );
  $form['details']['server'] = array(
    '#markup' => pegasus_admin_render_server_table(array($server_name => $server_config), FALSE),
  );

  return $form;
}

/**
 * Form constructor to test a server pull request.
 *
 * @ingroup forms
 */
function pegasus_admin_server_test_pull_form($form, $form_state) {

  $form = array();
  $form['pull_group'] = array(
    '#title' => t('Test Pull request'),
    '#type' => 'fieldset',
  );
  $form['pull_group']['id'] = array(
    '#type' => 'textfield',
    '#title' => t('ID to pull'),
    '#required' => TRUE,
  );
  $form['pull_group']['type'] = array(
    '#type' => 'textfield',
    '#title' => t('Collection'),
    '#default_value' => 'node',
    '#required' => TRUE,
  );
  $form['pull_group']['write'] = array(
    '#type' => 'checkbox',
    '#title' => t('Save as well as pulling'),
  );
  $form['pull_group']['submit'] = array(
    '#id' => 'import',
    '#type' => 'submit',
    '#value' => t('Go get it!'),
  );
  $form['pull_group']['queue'] = array(
    '#id' => 'queue',
    '#type' => 'submit',
    '#value' => t('Queue'),
  );

  return $form;
}

/**
 * Form submission handler for pegasus_admin_server_test_pull_form.
 *
 * @see pegasus_admin_server_test_pull()
 */
function pegasus_admin_server_test_pull_form_submit($form, $form_state) {

  $output = '';
  $result = t('No result');

  $server_name = str_replace('-', '_', check_plain(arg(5)));

  $server_config = pegasus_get_server_configuration($server_name);

  if (empty($server_config)) {
    drupal_set_message('Invalid server name provided', 'error');
    return FALSE;
  }

  if (empty($form_state['values']['id'])) {
    drupal_set_message('Please provide an ID', 'error');
    return FALSE;
  }

  $server = pegasus_get_client($server_name);
  $id = check_plain($form_state['values']['id']);
  $type = check_plain($form_state['values']['type']);

  $request = pegasus_event_create_default();
  $request->setSourceId($id);
  $request->setAction('pull');
  $request->setSourceType($type);
  $request->setServer($server_name);

  switch ($form_state['triggering_element']['#id']) {
    case 'import':
      drupal_set_message('Attempting to load ' . $id . ' from the remote server.');

      // Turn on debugging.
      $server->debugOn();

      global $user;
      $request->setRequestingUser($user->uid);

      $result = $server->request('pull', $request, array('alter' => TRUE));

      if (isset($form_state['values']['write']) && $form_state['values']['write'] == 1) {
        drupal_set_message('Attempting to save the imported entity...');
        pegasus_import_entities($server_name, $type, $id);
      }

      break;

    case 'queue':

      $request->queue();
      $result = t('Entity queued for import.');

      break;
  }

  if (function_exists('dpm')) {
    dpm($result);
  }
  else {
    $output = '<pre>' . print_r($result) . '</pre>';
  }

  // All done, turn off Debugging.
  $server->debugOff();

  return $output;
}

/**
 * Form constructor to test a server connection.
 *
 * @see pegasus_admin_server_test_pull()
 */
function pegasus_admin_server_test_connection_form($form, $form_state) {
  $form = array();
  $form['test_conn'] = array(
    '#title' => t('Test connection'),
    '#type' => 'fieldset',
  );
  $form['test_conn']['content'] = array(
    '#markup' => '<p>Click the button to test your server connection</p>',
  );
  $form['test_conn']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Test'),
  );

  return $form;
}

/**
 * Form submit handler for pegasus_admin_server_test_connection_form.
 *
 * @see pegasus_admin_server_test_pull()
 */
function pegasus_admin_server_test_connection_form_submit($form, $form_state) {

  $server_name = str_replace('-', '_', check_plain(arg(5)));

  $server = pegasus_get_client($server_name);

  if ($server instanceof \Drupal\pegasus\Interfaces\TestingInterface) {
    $test = $server->testConnection();
    if ($test == TRUE) {
      drupal_set_message('Test connection returned OK');
    }
    else {
      drupal_set_message('Test connection FAILED', 'error');
    }
  }
  else {
    drupal_set_message('Server type does not support testing.', 'warning');
  }
}

/**
 * Form constructor to set server options.
 */
function pegasus_admin_server_events_form($form, $form_state) {
  $server_name = str_replace('-', '_', check_plain(arg(5)));
  $event_queue = pegasus_queue_handler($server_name, 'events');

  $form = array();
  $form['options'] = array(
    '#title' => t('Event tracking'),
    '#type' => 'fieldset',
  );
  $form['options']['last_event'] = array(
    '#markup' => '<p><strong>' . t('Last requested event ') . $event_queue->eventQueueMarker() . ':</strong> ' . $event_queue->lastRealEvent() . '</p>',
  );
  $form['options']['reset_events'] = array(
    '#type' => 'select',
    '#title' => t('Event queue mode'),
    '#options' => array(
      PEGASUS_QUEUE_STATE_NORMAL => t('Normal'),
      PEGASUS_QUEUE_STATE_CLEAN => t('Pull all events'),
    ),
    '#default_value' => variable_get('pegasus_event_state_' . $server_name, PEGASUS_QUEUE_STATE_NORMAL),
  );
  $form['options']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Form submit handler for pegasus_admin_server_events_form.
 *
 * @see pegasus_admin_server_test_pull()
 */
function pegasus_admin_server_events_form_submit($form, $form_state) {

  $server_name = str_replace('-', '_', check_plain(arg(5)));
  $config = pegasus_get_server_configuration($server_name);
  // Check we actually have a valid configuration.
  if (!empty($config)) {

    // Set the event queue state for this server.
    // Possible values are 'normal' or 'clean', which always loads all events.
    $event_state = $form_state['values']['reset_events'];
    variable_set('pegasus_event_state_' . $server_name, $event_state);
    drupal_set_message('Event queue mode saved');
  }

}

/**
 * Path callback to flush a server's settings.
 */
function pegasus_admin_server_admin_flush($server_config) {

  pegasus_flush_client_settings($server_config['name']);
  drupal_goto(PEGASUS_ADMIN_BASE_PATH . '/servers');
}

/**
 * Path callback to fetch events for a server.
 */
function pegasus_admin_server_admin_fetch_events($server_config) {

  pegasus_events_fetch($server_config['name']);
  pegasus_queue_execute($server_config['name'] . '_events');
  drupal_set_message('Executed Events queue');
  drupal_goto(PEGASUS_ADMIN_BASE_PATH . '/servers');
}

/**
 * Path callback to delete a queue item.
 *
 * @param \Drupal\pegasus\Queue\QueueInterface $queue_handler
 *   The Queue Handler
 * @param string $item_id
 *   The item to delete.
 */
function pegasus_admin_server_admin_delete_queue_item($queue_handler, $item_id) {
  $queue_handler->getQueue()->deleteItem($item_id);
  drupal_set_message('Deleted item with ID ' . $item_id);
  drupal_goto(PEGASUS_ADMIN_BASE_PATH . '/queues/view/' . $queue_handler->getQueueName());
}
