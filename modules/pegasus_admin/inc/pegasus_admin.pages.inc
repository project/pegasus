<?php
/**
 * @file
 * Page callbacks for the pegasus_admin module.
 *
 * @license GPL v2 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Chris Skene chris at previousnext dot com dot au
 */

/**
 * Page callback for the overview page.
 *
 * @return string
 *   The output of the overview page.
 */
function pegasus_admin_overview_page() {
  $output = '';

  $form = array();

  // Load pages declared by features.
  $features = module_invoke_all('pegasus_admin_features');
  if (!empty($features)) {
    foreach ($features as $key => $feature) {
      $form['feature_' . $key] = array(
        '#type' => 'fieldset',
        '#title' => $feature['title'],
        '#value' => '<p>' . $feature['description'] . '</p>' . l(t('More...'), PEGASUS_ADMIN_BASE_PATH . '/' . $key),
      );
    }
  }

  if (empty($form)) {
    $output = 'No functionality available. Perhaps you need to enable some modules';
  }
  else {
    $output = drupal_render($form);
  }

  return $output;
}

/**
 * Form callback for pegasus_admin_options_form().
 *
 * @ingroup 'forms'
 */
function pegasus_admin_options_form($form, $form_state) {
  $form = array();

  $form['debugging'] = array(
    '#type' => 'fieldset',
    '#title' => t('Debugging'),
  );
  $form['debugging']['debug'] = array(
    '#type' => 'select',
    '#options' => array(
      'off' => t('Off'),
      'on' => t('On'),
    ),
    '#default_value' => variable_get('pegasus_debugging', 'off'),
    '#title' => t('Debug messages'),
    '#description' => t('When debugging is on, the module will output additional information on requests and process to the screen and log. It is not recommended to use this in a production environment.'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Submit handler for pegasus_admin_options_form().
 *
 * @see pegasus_admin_options_form()
 */
function pegasus_admin_options_form_submit($form, $form_state) {

  if (isset($form_state['values']['debug'])) {
    variable_set('pegasus_debugging', $form_state['values']['debug']);
  }

  drupal_set_message('The options were saved');
}

/**
 * Tab form callback for entities.
 */
function pegasus_admin_entity_tab($form, &$form_state, $entity_type, $entity) {
  $form = array();

  if (!is_object($entity)) {
    $entities = entity_load($entity_type, array($entity));
    $entity = reset($entities);
  }

  if (property_exists($entity, 'uuid')) {
    $form['details'] = array(
      '#type' => 'fieldset',
      '#title' => t('Entity Details'),
    );
    $form['details']['uuid'] = array(
      '#markup' => '<p><strong>UUID:</strong> ' . $entity->uuid . '</p>',
    );
  }

  $form['status'] = array(
    '#type' => 'fieldset',
    '#title' => t('Pegasus Status'),
  );

  $sync_implementations = pegasus_sync_type_info();

  if (!empty($sync_implementations)) {

    $bundle = pegasus_get_entity_bundle($entity_type, $entity);
    $sync_status = pegasus_entity_sync_status($entity_type, $bundle, $entity->uuid);

    if ($sync_status->isSynched()) {

      $profiles = $sync_status->getSyncProfiles();
      if (!empty($profiles)) {

        $header = array(
          'status' => array(
            'data' => t('Status'),
          ),
          'source' => array(
            'data' => t('Source'),
          ),
          'source_id' => array(
            'data' => t('Source ID'),
          ),
          'type' => array(
            'data' => t('Type'),
          ),
          'handler' => array(
            'data' => t('Handler'),
          ),
          'operations' => array(
            'data' => t('Operations'),
          ),
        );
        $rows = array();

        foreach ($profiles as $index => $profile) {

          $rows[$index] = array();

          $handler_name = $sync_status->getProfileHandler($index);

          $sync = t('Synched');
          if ($sync_status->getProfileType($index)) {
            switch ($sync_status->getProfileType($index)) {

              case PEGASUS_SYNC_TYPE_PUSH:
                $sync = t('Pushed to remote');
                break;

              case PEGASUS_SYNC_TYPE_PENDING_PUSH:
                $sync = t('Pending push to remote');
                break;

              case PEGASUS_SYNC_TYPE_PULL:
                $sync = t('Pulled from remote master');
                break;

              case PEGASUS_SYNC_TYPE_BOTH:
                $sync = t('Two-way sync');
                break;
            }
          }

          $rows[$index]['status'] = array('data' => $sync);

          // Source.
          if ($sync_status->getProfileSource($index)) {
            $source = $sync_status->getProfileSource($index);
          }
          else {
            $source = '<em>' . t('Unknown') . '</em>';
          }
          $rows[$index]['source'] = array('data' => $source);

          // Source ID.
          if ($sync_status->getProfileSourceId($index)) {
            $id = $sync_status->getProfileSourceId($index);
          }
          else {
            $id = '<em>' . t('Unknown') . '</em>';
          }
          $rows[$index]['source_id'] = array('data' => $id);

          // Sync type.
          if ($sync_status->getProfileType($index)) {
            $type = $sync_status->getProfileType($index);
          }
          else {
            $type = '<em>' . t('Unknown') . '</em>';
          }
          $rows[$index]['type'] = array('data' => $type);

          // Handler.
          // Get a title for this implementation.
          $title = $handler_name;
          if (array_key_exists($handler_name, $sync_implementations)) {
            isset($sync_implementations[$handler_name]['name']) ? $title = $sync_implementations[$handler_name]['name'] : $title = $handler_name;
          }
          $rows[$index]['handler'] = array('data' => $title);

          // Operations.
          $ops_output = '';
          if (array_key_exists('operations', $sync_implementations[$handler_name])) {
            $ops = array();
            foreach ($sync_implementations[$handler_name]['operations'] as $operation_key => $operation) {

              // If the 'actions' key is supplied, only show actions which match
              // the current profile type.
              if (array_key_exists('actions', $operation)) {
                if (!in_array($sync_status->getProfileType($index), $operation['actions'])) {
                  continue;
                }
              }

              if (array_key_exists('callback', $operation) && function_exists($operation['callback'])) {
                if (array_key_exists('name', $operation)) {
                  $title = $operation['name'];
                }
                else {
                  $title = $operation_key;
                }
                $ops[] = l($title, current_path() . '/action/' . $operation_key, array(
                  'query' => array(
                    'path' => current_path(),
                    'source' => $sync_status->getProfileSource($index),
                    'handler' => $sync_status->getProfileHandler($index),
                  ),
                ));
              }
            }
            if (!empty($ops)) {
              $ops_output = implode(' | ', $ops);
            }
          }
          $rows[$index]['operations'] = $ops_output;
        }

        $vars = array(
          'header' => $header,
          'rows' => $rows,
        );
        $form['status']['setting'] = array(
          '#markup' => theme('table', $vars),
        );
      }
    }
    else {
      $form['status']['setting'] = array(
        '#markup' => t('Not synchronised.'),
      );
    }

    $form['status']['links'] = array(
      '#markup' => '<p>' . l(t('Queues'), 'admin/config/services/pegasus/queues') . '</p>',
    );
  }

  return $form;
}

/**
 * Menu callback for entity tab actions.
 *
 * @param string $entity_type
 *   The entity type.
 * @param object $entity
 *   The entity.
 * @param string $action
 *   The action
 */
function pegasus_admin_entity_action($entity_type, $entity, $action) {

  // Load a redirect path.
  $params = drupal_get_query_parameters();
  if (array_key_exists('path', $params)) {
    $redirect = check_plain($params['path']);
  }
  else {
    $redirect = '/';
  }

  // XSS security check: is the path valid? If not, fail.
  if (!drupal_valid_path($redirect, TRUE)) {
    drupal_set_message('Invalid path supplied for redirect.');
    drupal_goto('');
  }

  // If no UUID is present, we can't act on this item.
  if (!property_exists($entity, 'uuid')) {
    drupal_set_message('Invalid entity provided. No UUID found.');
    drupal_goto($redirect);
  }

  // Load required sync info.
  $sync_implementations = pegasus_sync_type_info();
  $bundle = pegasus_get_entity_bundle($entity_type, $entity);
  $sync_status = pegasus_entity_sync_status($entity_type, $bundle, $entity->uuid);

  // If not actually synched, return.
  if (!$sync_status->isSynched()) {
    drupal_set_message('Entity is not synchronised. No action taken.');
    drupal_goto($redirect);
  }

  // Add the sync profile to the $data.
  $profiles = $sync_status->getSyncProfiles();
  $data = array();
  foreach ($profiles as $profile) {
    if ($profile['handler'] == $params['handler'] && $profile['source'] == $params['source']) {
      $data = $profile;
    }
  }

  // Invoke the action.
  pegasus_invoke_admin_action($action, $params['handler'], $params['source'], $entity_type, $entity, $data);
  drupal_set_message('Executed the action "' . $sync_implementations[$params['handler']]['operations'][$action]['name'] . '".');

  drupal_goto($redirect);
}