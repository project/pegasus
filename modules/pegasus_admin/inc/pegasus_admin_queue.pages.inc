<?php
/**
 * @file
 * Provides pages for the pegasus queue manager.
 *
 * @copyright Copyright(c) 2012 Previous Next Pty Ltd
 * @license GPL v2 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Lee Rowlands larowlan at previousnext dot com dot au
 * @author Chris Skene chris at previousnext dot com dot au
 *
 * This file was freely adapted for Pegasus from Lee Rowland's
 * Queue Manager module.
 */

/**
 * Queue form handler.
 */
function pegasus_queue_mgr_page($form, &$form_state) {
  // Initialize.
  if ($form_state['rebuild']) {
    $form_state['input'] = array();
  }
  if (empty($form_state['storage'])) {
    // First step, so start with our overview form.
    $form_state['storage'] = array(
      'step' => 'pegasus_queue_mgr_overview_form',
    );
  }
  // Return the form from the current step.
  $function = $form_state['storage']['step'];
  $form = $function($form, $form_state);
  return $form;
}

/**
 * Submit handler for the queue overview form.
 */
function pegasus_queue_mgr_page_submit($form, &$form_state) {
  $values = $form_state['values'];
  // Get submitted queues to act on.
  $queues = array_filter($values['queues']);
  if (empty($queues)) {
    // Nothing to do.
    return;
  }
  if (isset($values['step_submit'])) {
    // Pass off to step submit handler.
    $function = $values['step_submit'];
    $function($form, $form_state, $queues);
  }
  return;
}

/**
 * Overview of queues.
 */
function pegasus_queue_mgr_overview_form() {
  $servers = pegasus_get_server_configurations();

  $options = array();
  $header = array(
    'title' => array('data' => t('Queue')),
    'items' => array('data' => t('Number of items')),
    'claimed' => array('data' => t('Claimed items')),
    'view' => array('data' => t('View items')),
  );

  // Get queues.
  $queues = pegasus_queue_get_queues();
  if (!empty($queues)) {
    foreach ($queues as $id => $queue) {
      $name_parts = explode(': ', $queue['title']);
      $server_title = $servers[$name_parts[0]]['title'];
      $queue_name = $name_parts[1];

      $options[$id] = array(
        'title' => check_plain($server_title . ': ' . $queue_name),
        'items' => $queue['items'],
        'claimed' => $queue['claimed'],
        'view' => l(t('View'), 'admin/config/services/pegasus/queues/view/' . $id),
      );
    }
  }

  $form['queues'] = array(
    '#type' => 'tableselect',
    '#header' => $header,
    '#options' => $options,
    '#empty' => t('No queues exist.'),
  );

  // Option to run batch.
  $form['batch'] = array(
    '#type' => 'submit',
    '#value' => t('Process now'),
  );

  // Option to clear stuck items.
  $form['clean'] = array(
    '#type' => 'submit',
    '#value' => t('Reset claimed items'),
  );

  // Option to delete queue.
  $form['delete'] = array(
    '#type' => 'submit',
    '#value' => t('Clear queues'),
  );
  // Specify our step submit callback.
  $form['step_submit'] = array('#type' => 'value', '#value' => 'pegasus_queue_mgr_overview_submit');
  return $form;
}

/**
 * Overview submit handler.
 */
function pegasus_queue_mgr_overview_submit($form, &$form_state, $queues) {
  $values = $form_state['values'];

  // Switch off submitted action.
  switch ($values['op']) {
    case $values['batch']:
      // Process queue(s) with batch.
      $defined_queues = pegasus_queue_get_queues();
      $intersect = array_intersect(array_keys($defined_queues), $queues);
      foreach ($intersect as $id) {
        pegasus_queue_execute($id);
      }
      break;

    case $values['clean']:
      // Process queue(s) with batch.
      $defined_queues = pegasus_queue_get_queues();
      $intersect = array_intersect(array_keys($defined_queues), $queues);
      foreach ($intersect as $id) {
        $queue = pegasus_queue_load($id);
        if ($queue->numberOfItems() > 0) {
          $queue->releaseAllItems();
        }
      }
      drupal_set_message('All queue items reset and ready for processing');
      break;

    case $values['delete']:
      // Confirm before deleting.
      $form_state['rebuild'] = TRUE;
      $form_state['storage']['queues'] = $queues;
      $form_state['storage']['step'] = 'pegasus_queue_mgr_confirm_delete';
      break;

  }
  return;
}

/**
 * Confirm form for deleting queues.
 */
function pegasus_queue_mgr_confirm_delete($form, &$form_state) {
  $form['queues'] = array('#type' => 'value', '#value' => $form_state['storage']['queues']);
  $description = t('All items in each queue will be deleted. This operation cannot be undone.');

  // Specify our step submit callback.
  $form['step_submit'] = array('#type' => 'value', '#value' => 'pegasus_queue_mgr_delete_submit');
  return confirm_form($form,
    format_plural(count($form_state['storage']['queues']), 'Are you sure you want to clear the queue?', 'Are you sure you want to clear @count queues?'),
    'admin/config/services/pegasus/queues',
    $description,
    t('Clear'),
    t('Cancel')
  );
}

/**
 * Submit handler for deleting queues.
 */
function pegasus_queue_mgr_delete_submit($form, &$form_state) {
  $values = $form_state['values'];
  $queues = $values['queues'];
  foreach ($queues as $id) {
    $queue = pegasus_queue_load($id);
    $queue->deleteQueue();
  }
  drupal_set_message(format_plural(count($values['queues']), 'Queue deleted', '@count queues deleted'));
  return;
}
