<?php
/**
 * @file
 * API information for the Pegasus module
 *
 * @copyright Copyright(c) 2012 Previous Next Pty Ltd
 * @license GPL v2 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Chris Skene chris at previousnext dot com
 */

/**
 * Declare a server implementation.
 *
 *  Each server is defined as a single keyed array with the following keys:
 *  - title: The human readable name of the server
 *  - description: A description for the server
 *  - class: The class the server uses.
 *
 * @return array
 *   An array of server definitions.
 */
function hook_pegasus_server_servers() {
  $servers = array();
  $servers['example_server'] = array(
    'title' => t('Example server'),
    'description' => t('A simple server to demonstrate functionality'),
    'class' => 'ExamplePegasusServer',
  );

  return $servers;
}

/**
 * Declare a synchronisation type.
 *
 * Any module which wants to use pegasus to sync content should declare a sync
 * type. While it is possible for a module not to do this, and simply call
 * pegasus as needs be, this allows pegasus to check the module for item status
 * and other similar tasks.
 *
 * @return array
 *   An array defining the type.
 */
function hook_pegasus_sync_types() {
  $types = array();
  $types['pegasus_field'] = array(
    'name' => t('Pegasus Field'),
    'status callback' => 'pegasus_field_sync_status',
    'source entities callback' => 'pegasus_field_pegasus_entities_by_source',
  );

  return $types;
}

/**
 * Respond to a request on a specific server.
 *
 * @param mixed $result
 *   The result of the request.
 */
function hook_pegasus_SERVERNAME_ACTION_alter($result) {
  // Do something to the $result. No need to return.
}

/**
 * Give implementations an opportunity to alter the field implementaiton.
 *
 * This is called by PegasusClient::mapToEntity(), in order to determine which
 * fields are required on the target node. It should be implemented by whatever
 * module is handling field mapping (e.g. pegasus_field), on a per content type
 * basis.
 *
 * @param array $field_data
 *   Information about fields, and other properties. Keys include:
 *   'type'   The type of entity.
 *   'subtype'    The subtype or bundle.
 *   'server'   The server name.
 *   'fields'   The array of field data.
 */
function hook_pegasus_entity_fields_alter($field_data) {

}

/**
 * Perform additional mapping of entity properties from the source document map.
 *
 * @param \Drupal\pegasus\Core\PegasusInterface $server
 *   The server from which the source document was retrieved
 * @param \Drupal\pegasus\Event\EventInterface $event
 *   The entity being created
 * @param object $entity
 *   The target entity.
 * @param array $source_map
 *   The source document.
 */
function hook_pegasus_map_to_entity($server, $event, $entity, $source_map) {

}

/**
 * Provide field translation handler information for specific field types.
 *
 * Translation pairs should be handled as in the example, with a field type as
 * the key, and an object which implements \FieldHandlerInterface as the
 * value. This will be used to perform field translation on fields of the given
 * type. The entire array should be inside an array keyed by the machine name
 * of the server the fields are for.
 *
 * @param string $server_name
 *   The name of the server being called.
 * @param string $entity_type
 *   The entity type.
 * @param string|null $bundle
 *   The bundle name.
 *
 * @return array
 *   An array of field mapper information.
 */
function hook_pegasus_field_type_handlers($server_name, $entity_type, $bundle) {
  if ($server_name == 'my_server_machine_name') {
    return array(
      'my_server_machine_name' => array(
        // References an object called myServerTextHandlerName.
        'text' => 'myServerTextHandlerName',
      ),
    );
  }
}

/**
 * Provide field translation handler information for individual fields.
 *
 * Translation pairs should be handled as in the example, with a field name as
 * the key, and an object which implements \FieldHandlerInterface as the
 * value. This will be used to perform field translation on fields of the given
 * type. The entire array should be inside an array keyed by the machine name
 * of the server the fields are for.
 *
 * In some implementations, this may simply retrieve field handler information
 * from elsewhere and map it to what pegasus module expects.
 *
 * @param string $server_name
 *   The name of the server being called.
 * @param string $entity_type
 *   The entity type.
 * @param string|null $bundle
 *   The bundle name.
 *
 * @return array
 *   An array of field mapper information.
 */
function hook_pegasus_field_handlers($server_name, $entity_type, $bundle) {
  if ($server_name == 'my_server_machine_name') {
    return array();
  }
}

/**
 * Declare server types.
 *
 * Every server must be of a server_type. For custom servers, this is
 * automatically implemented by the Pegasus API, however for modules
 * like the Server Manager, individual server configuration require a type in
 * order to be recognised (and configurable);
 *
 * The type array is keyed by the type machine name, and contains three keys:
 *  'title' - The server type's human readable title
 *  'description' - a description of the server type
 *  'class' - The class used by the server type
 */
function hook_pegasus_server_types() {
  $types = array();

  $types['drupal'] = array(
    'title' => t('A Drupal-based server type'),
    'description' => t('Defines a server type which connects to a Drupal content repository'),
    'class' => 'Drupal\pegasus_server_drupal\Client\PegasusDrupalClient',
  );

  return $types;
}

/**
 * Provide a Pegasus API feature for inclusion under Pegasus menu item.
 *
 * To provide some flexibility around which modules are enabled in the Pegasus
 * suite, this hook allows implementers to specify a "feature" to be added to
 * the Pegasus page, and a page callback which provides settings for
 * that feature (for example, the pegasus_admin module itself provides the
 * server listing this way).
 *
 * Possible keys include:
 *  - 'title': The title of the feature. This should be translated.
 *  - 'page callback': A callback for the page, if drupal_get_form is provided,
 *        then the 'page arguments' key should also be specified, ala hook_menu.
 *  - 'file': (optional) The file to load the callback from, relative to the
 *        root of the current module.
 *  - 'description': A description.
 *  - 'weight': (optional) The menu weight.
 *
 * @return array
 *   An array of feature definitions.
 */
function hook_admin_pegasus_admin_features() {
  $features = array();

  $features['servers'] = array(
    'title' => t('Servers'),
    'page callback' => 'pegasus_admin_servers_page',
    'file' => 'inc/pegasus_admin.pages.inc',
    'description' => t('Server configuration information'),
    'weight' => -50,
  );

  return $features;
}

/**
 * Declare information about entity handlers.
 */
function hook_pegasus_entity_handlers() {
  $handlers = array();
  $handlers['node'] = array(
    'name' => t('Node'),
    'class' => '\Drupal\pegasus\Entity\NodeEntityHandler',
  );

  return $handlers;
}
