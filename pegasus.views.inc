<?php
/**
 * @file
 * Views integration for pegasus.
 *
 * @copyright Copyright(c) 2012 Previous Next Pty Ltd
 * @license GPL v2 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Chris Skene chris at previousnext dot com dot au
 */

/**
 * Implements hook_views_data().
 */
function pegasus_views_data() {

  // The 'group' index will be used as a prefix in the UI for any of this
  // table's fields, sort criteria, etc. so it's easy to tell where they came
  // from.
  $data['pegasus_queue']['table']['group'] = t('Pegasus Queue');

  // Define this as a base table.
  $data['pegasus_queue']['table']['base'] = array(
    'field' => 'item_id',
    'title' => t('Pegasus Queue'),
    'help' => t('Access the Pegasus queue.'),
    'weight' => -10,
  );

  // Queue ID field.
  $data['pegasus_queue']['item_id'] = array(
    'title' => t('Queue ID'),
    'help' => t('The primary queue identifier.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  // Queue name.
  $data['pegasus_queue']['name'] = array(
    'title' => t('Queue name'),
    'help' => t('Name of the queue.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  // Queue name.
  $data['pegasus_queue']['type'] = array(
    'title' => t('Type'),
    'help' => t('The type of entity.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  // Queue name.
  $data['pegasus_queue']['bundle'] = array(
    'title' => t('Bundle'),
    'help' => t('The entity bundle.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  // Queue name.
  $data['pegasus_queue']['source'] = array(
    'title' => t('Server'),
    'help' => t('The source server.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  // Source ID.
  $data['pegasus_queue']['source_id'] = array(
    'title' => t('Source ID'),
    'help' => t('Identifier of the source item.'),
    'field' => array(
      'handler' => 'views_handler_field',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  // Event data.
  $data['pegasus_queue']['data'] = array(
    'title' => t('Data'),
    'help' => t('Event data.'),
    'field' => array(
      'handler' => 'views_handler_field',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  // Example timestamp field.
  $data['pegasus_queue']['expire'] = array(
    'title' => t('Lease expiration'),
    'help' => t('Timestamp of the claim lease expiration.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  // Example timestamp field.
  $data['pegasus_queue']['created'] = array(
    'title' => t('Created'),
    'help' => t('Timestamp of the items creation.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  return $data;
}