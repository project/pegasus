<?php
/**
 * @file
 * Defines methods required for the Queue interface.
 *
 * @license GPL v2 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Chris Skene chris at previousnext dot com dot au
 */

namespace Drupal\pegasus\Queue;

/**
 * Defines an interface for interacting with Drupal queues on the server object.
 */
interface QueueInterface {

  /**
   * Populate the queue.
   *
   * @param array $events
   *   An array of event items retrieved from the PegasusClient.
   */
  public function populateQueue($events);

  /**
   * Set the server (dependency injection).
   *
   * @param string|\Drupal\pegasus\Core\PegasusInterface $server
   *   A pegasus server.
   */
  public function setServer($server);

  /**
   * Get the queue for an action.
   *
   * Given an action (as defined on the server), return the relevant Queue.
   *
   * @param string $action
   *   The queue for a given action.
   *
   * @return \DrupalReliableQueueInterface
   *   Instance of SystemQueue object.
   */
  public function loadQueue($action);

  /**
   * Make a queue name.
   *
   * @param string $name
   *   The name to be used as part of the queue name. Usually, the method being
   *   called.
   *
   * @return string
   *   A queue name.
   */
  public function makeQueueName($name);

  /**
   * Queue Item for processing.
   *
   * @param \Drupal\pegasus\Event\EventInterface $event
   *   The item to be pushed to the queue.
   * @param string $method
   *   The method for the item, eg push, pull.
   *
   * @return bool
   *   Result of the queuing operation.
   */
  public function queueItem(\Drupal\pegasus\Event\EventInterface $event, $method);

  /**
   * Return the PegasusQueue directly.
   *
   * @return \Drupal\pegasus\Queue\PegasusQueue
   *   The queue.
   */
  public function getQueue();

  /**
   * Get the name of this queue.
   *
   * @return string
   *   The queue name.
   */
  public function getQueueName();
}
