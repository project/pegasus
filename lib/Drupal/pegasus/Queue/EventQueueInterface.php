<?php
/**
 * @file
 * Defines an interface for interacting with pegasus_event.
 *
 * @license GPL v2 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Chris Skene chris at previousnext dot com dot au
 */

namespace Drupal\pegasus\Queue;

/**
 * Defines methods required to interact all with pegasus_event's event handling.
 */
interface EventQueueInterface {

  /**
   * Retrieve the event queue.
   *
   * @param string $last_event
   *   (optional) ID of the first event to retrieve.
   * @param int $count
   *   (optional) Number of events to retrieve.
   *
   * @return array
   *   An array of events.
   */
  public function getEvents($last_event = NULL, $count = 0);

  /**
   * Maximum number of events to return in one go.
   *
   * @return int
   *   The number of events to add the event queue in one go (e.g. 100).
   *   Defaults to 10.
   */
  public function maxEvents();

  /**
   * Return the key to use as a queue marker.
   *
   * Valid values are currently 'id' or 'timestamp'.
   *
   * @return string
   *   A key.
   */
  public function eventQueueMarker();

  /**
   * Return the last event processed by the event queue.
   *
   * @return string
   *   The value of the last processed event.
   */
  public function lastEvent();
}
