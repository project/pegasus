<?php
/**
 * @file
 * Provides a customised queue class for Pegasus.
 *
 * @copyright Copyright(c) 2012 Previous Next Pty Ltd
 * @license GPL v2 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Chris Skene chris at previousnext dot com dot au
 */

namespace Drupal\pegasus\Queue;

/**
 * A customised queue class.
 *
 * The main difference between this and the standard system queue is that we add
 * extra properties to the queue table so we can manipulate it more easily, and
 * consequently we use a separate table as well.
 */
class PegasusQueue
  extends \SystemQueue
  implements \DrupalReliableQueueInterface {

  /**
   * Instead of reusing an existing event item, push it to the end of the queue.
   *
   * @param \Drupal\pegasus\Event\EventInterface $event
   *   The event to queue
   */
  public function createDelayedItem($event) {
    $this->deleteByUuid($event->getSourceId());

    $this->createItem($event);
  }

  /**
   * Overrides SystemQueue::createItem().
   *
   * @param \Drupal\pegasus\Event\EventInterface $event
   *   The event to queue
   *
   * @return bool
   *   TRUE, if the item is created, or FALSE.
   */
  public function createItem($event) {

    $id = $event->getSourceId();
    // Queues don't need more than the latest queued update for a given
    // source item.
    $num_rows = db_select('pegasus_queue')
      ->condition('source_id', $event->getSourceId())
      ->countQuery()
      ->execute()
      ->fetchField();

    // Only add an queue item if none exists.
    if ($num_rows < 1) {

      // Insert the latest item.
      $query = db_insert('pegasus_queue')
        ->fields(array(
        'name' => $this->name,
        'type' => $event->getSourceType(),
        'bundle' => $event->getSourceSubtype(),
        'source' => $event->getServer(),
        'source_id' => $event->getSourceId(),
        'data' => serialize($event),
        // We cannot rely on REQUEST_TIME because many items might be created
        // by a single request which takes longer than 1 second.
        'created' => time(),
      ));

      $result = (bool) $query->execute();
      if ($result == TRUE) {
        return PEGASUS_QUEUE_QUEUE_SUCCESS;
      }

      return PEGASUS_QUEUE_QUEUE_FAIL;
    }

    return PEGASUS_QUEUE_QUEUE_PASS;
  }

  /**
   * Overrides SystemQueue::claimItem().
   */
  public function claimItem($lease_time = 30) {
    // Claim an item by updating its expire fields. If claim is not successful
    // another thread may have claimed the item in the meantime. Therefore loop
    // until an item is successfully claimed or we are reasonably sure there
    // are no unclaimed items left.
    while (TRUE) {
      $item = db_query_range('SELECT data, item_id FROM {pegasus_queue} q WHERE expire = 0 AND name = :name ORDER BY created ASC', 0, 1, array(':name' => $this->name))->fetchObject();
      if ($item) {
        // Try to update the item. Only one thread can succeed in UPDATEing the
        // same row. We cannot rely on REQUEST_TIME because items might be
        // claimed by a single consumer which runs longer than 1 second. If we
        // continue to use REQUEST_TIME instead of the current time(), we steal
        // time from the lease, and will tend to reset items before the lease
        // should really expire.
        $update = db_update('pegasus_queue')
          ->fields(array(
          'expire' => time() + $lease_time,
        ))
          ->condition('item_id', $item->item_id)
          ->condition('expire', 0);
        // If there are affected rows, this update succeeded.
        if ($update->execute()) {
          $item->data = unserialize($item->data);
          return $item;
        }
      }
      else {
        // No items currently available to claim.
        return FALSE;
      }
    }
  }

  /**
   * Overrides SystemQueue::numberOfItems().
   */
  public function numberOfItems() {
    return db_query('SELECT COUNT(item_id) FROM {pegasus_queue} WHERE name = :name', array(':name' => $this->name))->fetchField();
  }

  /**
   * Get a count of unclaimed items.
   *
   * @return int
   *   The number of unclaimed items.
   */
  public function unclaimedItems() {
    $num_rows = db_select('pegasus_queue')
      ->condition('name', $this->name)
      ->condition('expire', 0)
      ->countQuery()
      ->execute()
      ->fetchField();

    return $num_rows;
  }

  /**
   * Get a count of the claimed items.
   *
   * @return int
   *   The number of claimed items.
   */
  public function claimedItems() {
    $num_rows = db_select('pegasus_queue')
      ->condition('name', $this->name)
      ->condition('expire', 0, '>')
      ->countQuery()
      ->execute()
      ->fetchField();

    return $num_rows;
  }

  /**
   * Overrides SystemQueue::releaseItem().
   */
  public function releaseItem($item) {
    $update = db_update('pegasus_queue')
      ->fields(array(
      'expire' => 0,
    ))
      ->condition('item_id', $item->item_id);
    return $update->execute();
  }

  /**
   * Release all expired items.
   */
  public function releaseAllItems() {
    $query = db_update('pegasus_queue')
      ->condition('name', $this->name)
      ->fields(array('expire' => 0))
      ->execute();
  }

  /**
   * Overrides SystemQueue::deleteItem().
   */
  public function deleteItem($item) {
    if (is_object($item)) {
      property_exists($item, 'id') ? $id = $item->id : $id = $item->item_id;
    }
    else {
      $id = $item;
    }
    db_delete('pegasus_queue')
      ->condition('item_id', $id)
      ->execute();
  }

  /**
   * Delete an item by its UUID.
   *
   * @param string $uuid
   *   A valid UUID.
   */
  public function deleteByUuid($uuid) {
    db_delete('pegasus_queue')
      ->condition('source_id', $uuid)
      ->execute();
  }

  /**
   * Given an event, delete its queue record.
   *
   * @param \Drupal\pegasus\Event\EventInterface $event
   *   A pegasus event.
   */
  public function deleteByEvent($event) {
    $del_query = db_delete('pegasus_queue')
      ->condition('source_id', $event->getSourceId())
      ->execute();
  }

  /**
   * Overrides SystemQueue::deleteQueue().
   */
  public function deleteQueue() {
    db_delete('pegasus_queue')
      ->condition('name', $this->name)
      ->execute();
  }

  /**
   * Get the queue name.
   *
   * @return string
   *   The queue name.
   */
  public function name() {
    return $this->name;
  }

}
