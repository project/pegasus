<?php
/**
 * @file
 * Contains a handler for Queues.
 *
 * @license GPL v2 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Chris Skene chris at previousnext dot com dot au
 */

namespace Drupal\pegasus\Queue;

/**
 * Handler for event queues.
 */
class EventQueueHandler
  extends QueueHandler
  implements \Drupal\pegasus\Queue\EventQueueInterface {

  /** METHODS FROM EventProcessingInterface *********************************/

  /**
   * Retrieve the event queue.
   *
   * @param string $last_event
   *   (optional) ID of the first event to retrieve.
   * @param int $count
   *   (optional) Number of events to retrieve.
   *
   * @return array
   *   An array of events.
   */
  public function getEvents($last_event = NULL, $count = 0) {
    $last_id = $this->lastEvent();
    return $this->server->requestEvents($last_id);
  }

  /**
   * Maximum number of events to return in one go.
   *
   * @return int
   *   The number of events to add the event queue in one go (e.g. 100).
   *   Defaults to 10.
   */
  public function maxEvents() {
    return 10;
  }

  /**
   * Return the key to use as a queue marker.
   *
   * Valid values are currently 'id' or 'timestamp'.
   *
   * @return string
   *   A key.
   */
  public function eventQueueMarker() {
    return 'timestamp';
  }

  /**
   * Return the last event processed by the event queue.
   *
   * @return string
   *   The value of the last processed event.
   */
  public function lastEvent() {
    $marker = $this->server->eventQueueMarker();
    return $this->getLastItem($marker);
  }

  /**
   * Return the actual last event processed by the event queue.
   *
   * @return string
   *   The value of the last processed event.
   */
  public function lastRealEvent() {
    $marker = $this->server->eventQueueMarker();
    return $this->getLastItem($marker, TRUE);
  }

  /** OVERRIDDEN METHODS ****************************************************/

  /**
   * Overrides QueueHandler::populateItems().
   */
  public function populateQueue($items) {

    if (!empty($items)) {

      // We'll keep track of the latest item, so we can save it's ID.
      $latest_item = array();
      $latest_item['timestamp'] = 0;
      $latest_item = pegasus_event_create($latest_item);

      $count = 0;
      foreach ($items as $event) {

        if (!$event instanceof \Drupal\pegasus\Event\EventInterface) {
          $event = pegasus_event_create($event);
        }

        // Set the server before queueing, so we can retrieve it later.
        $event->setServer($this->server->getName());

        // Add our item into the queue.
        $this->enqueue($event);

        // If this item is later than the latest_item, this item becomes
        // the latest_item.
        if ($event->getTimestamp() > $latest_item->getTimestamp()) {
          $latest_item = $event;
        }

        $count++;
      }

      $this->server->log('Queued %count items.', array('%count' => $count), WATCHDOG_NOTICE);

      // Set the last fetched items for the next queue run.
      $this->setLastItem('id', $latest_item->getId());
      $this->setLastItem('timestamp', $latest_item->getTimestamp());
    }
    else {
      $this->server->log('There were no new event items.');
      drupal_set_message('There were no new event items.');
    }
  }

  /**
   * Overrides QueueHandler::queueName().
   */
  public function makeQueueName($action_name) {
    $this->name = $this->server->getName() . '_events';
    return $this->name;
  }

  /** INTERNAL FUNCTIONS ****************************************************/

  /**
   * Reset timestamp/id counters for a server.
   */
  protected function resetHighWater() {
    $this->setLastItem('id', NULL);
    $this->setLastItem('timestamp', 0);
  }

  /**
   * Returns the identifier of the last fetched event item.
   *
   * @param string $key
   *   Identifier to set.
   * @param bool $ignore_clean
   *   If TRUE, return the actual event, not the default last event.
   *
   * @return string
   *   The items value.
   */
  protected function getLastItem($key, $ignore_clean = FALSE) {
    $condition = variable_get('pegasus_event_state_' . $this->server->getName(), PEGASUS_QUEUE_STATE_NORMAL);
    if ($condition == PEGASUS_QUEUE_STATE_CLEAN && $ignore_clean == FALSE) {
      return 1;
    }
    return variable_get('pegasus_event_last_' . $key . '_' . $this->server->getName(), 1);
  }

  /**
   * Sets the ID of the last_fetched timestamp.
   *
   * @param string $key
   *   Identifier to set.
   * @param string $id
   *   The identifier to set.
   */
  protected function setLastItem($key, $id) {
    variable_set('pegasus_event_last_' . $key . '_' . $this->server->getName(), $id);
  }
}
