<?php
/**
 * @file
 * Contains a handler for Queues.
 *
 * @license GPL v2 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Chris Skene chris at previousnext dot com dot au
 */

namespace Drupal\pegasus\Queue;

/**
 * Handler for queues.
 */
class QueueHandler
  implements \Drupal\pegasus\Queue\QueueInterface {

  /**
   * The referencing server.
   *
   * @var \Drupal\pegasus\Core\PegasusInterface
   */
  protected $server;

  /**
   * The queue in use.
   *
   * @var \Drupal\pegasus\Queue\PegasusQueue
   */
  protected $queue;

  /**
   * Name of the queue we are managing.
   *
   * @var string
   */
  protected $name;

  /**
   * Constructor.
   *
   * @param string $server_name
   *   A pegasus server name.
   * @param string $action
   *   An action to load the queue for.
   */
  public function __construct($server_name = NULL, $action = NULL) {
    if (isset($server_name) && !empty($server_name)) {
      $server = pegasus_get_client($server_name);
      $this->setServer($server);
    }
  }

  /**
   * Set the server (dependency injection).
   *
   * @param string|\Drupal\pegasus\Core\PegasusInterface $server
   *   A pegasus server.
   */
  public function setServer($server) {

    if (is_string($server)) {
      $server = pegasus_get_client($server);
    }

    $this->server = $server;
  }

  /**
   * Helper to make a queue name.
   *
   * @param string $action_name
   *   Short name to use as the queue part. Usually the method.
   *
   * @return string
   *   The queue name.
   */
  public function makeQueueName($action_name) {
    $actions = $this->server->getActions();
    if (array_key_exists($action_name, $actions)) {

      // If a method is specified, we use that as the queue name.
      if (isset($actions[$action_name]['queue'])) {
        $queue_name = $actions[$action_name]['queue'];
      }
      else {
        $queue_name = $action_name;
      }

      return $this->server->getName() . '_' . $queue_name;
    }

    $this->server->log('Invalid queue name specified', WATCHDOG_ERROR);
  }

  /**
   * Get the queue for an action.
   *
   * Given an action (as defined on the server), return the relevant Queue.
   *
   * @param string $action
   *   The queue for a given action.
   *
   * @return \DrupalReliableQueueInterface
   *   Instance of SystemQueue object.
   */
  public function loadQueue($action) {

    $queue_name = $this->makeQueueName($action);

    if (!isset($this->queue)) {
      $this->queue = pegasus_queue_load($queue_name);
    }

    return $this->queue;
  }

  /**
   * Queue an Event item for processing.
   *
   * @param \Drupal\pegasus\Event\EventInterface $event
   *   The item to be pushed to the queue.
   * @param string $action
   *   The action for the item, eg push, pull.
   *
   * @return mixed
   *   Result of the queuing operation.
   */
  public function queueItem(\Drupal\pegasus\Event\EventInterface $event, $action) {

    $this->loadQueue($action);

    $actions = $this->server->getActions();

    if (isset($actions[$action]['next action'])) {
      $event->setAction($actions[$action]['next action']);
    }
    else {
      $event->setAction($action);
    }

    $event->setServer($this->server->getName());

    $this->enqueue($event);
  }

  /**
   * Populate the queue.
   *
   * @param array $events
   *   An array of items retrieved from the PegasusClient.
   */
  public function populateQueue($events) {

    $count = 0;
    foreach ($events as $event) {

      // At this point, we're probably getting config arrays, so turn them into
      // typed events before we send them to the queue.
      if (!$event instanceof \Drupal\pegasus\Event\EventInterface) {
        $event = pegasus_event_create($event);
      }

      // Add our item into the queue.
      $this->enqueue($event);

      $count++;
    }

    $this->server->log('Queued %count items.', array('%count' => $count));
  }

  /**
   * Add an item to the queue.
   *
   * Among other things, this function exists to make sure queued items are
   * uniform.
   *
   * @param \Drupal\pegasus\Event\EventInterface $event
   *   An event to add to the queue.
   * @param bool $delayed
   *   If TRUE, the event will be pushed to the back of the queue.
   *
   * @return bool
   *   TRUE, if the event is queued successfully.
   */
  public function enqueue(\Drupal\pegasus\Event\EventInterface $event, $delayed = TRUE) {

    try {
      $this->setServer($event->getServer());

      // Invoke 'hook_pegasus_enqueue_preprocess'.
      module_invoke_all('pegasus_enqueue_preprocess', $event);

      // Give the server an opportunity to alter the event before queueing.
      $event = $this->server->alterEvent($event);

      // Add our item into the queue.
      if ($delayed == TRUE) {
        $this->queue->createDelayedItem($event);
      }
      $result = $this->queue->createItem($event);

      if ($result == PEGASUS_QUEUE_QUEUE_SUCCESS || $result == PEGASUS_QUEUE_QUEUE_PASS) {
        return TRUE;
      }
    }
    catch(\Drupal\pegasus\Exception\TerminateProcess $t) {
      pegasus_log($t->getMessage(), array(), WATCHDOG_NOTICE);
    }

    return FALSE;
  }

  /**
   * Return the PegasusQueue directly.
   *
   * @return \Drupal\pegasus\Queue\PegasusQueue
   *   The queue.
   */
  public function getQueue() {
    return $this->queue;
  }

  /**
   * Get the name of this queue.
   *
   * @return string
   *   The queue name.
   */
  public function getQueueName() {
    return $this->name;
  }

}
