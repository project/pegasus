<?php
/**
 * @file
 * Contains an interface for defining events.
 *
 * @license GPL v2 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Chris Skene chris at previousnext dot com dot au
 */

namespace Drupal\pegasus\Event;

/**
 * Defines an interface for handling events.
 */
interface EventInterface {

  /**
   * Set the action.
   *
   * @param string $action
   *   The action.
   */
  public function setAction($action);

  /**
   * Get the action.
   *
   * @return string
   *   The action.
   */
  public function getAction();

  /**
   * Set the state.
   *
   * @param string $state
   *   The state of the object.
   */
  public function setState($state);

  /**
   * Get the state.
   *
   * @return string
   *   The last state of the object.
   */
  public function getState();

  /**
   * Set the uuid.
   *
   * @param string $id
   *   (optional) A UUID. If not provided, one will be generated.
   */
  public function setId($id = NULL);

  /**
   * Get the uuid.
   *
   * @return string
   *   A uuid.
   */
  public function getId();

  /**
   * Set the source site ID.
   *
   * @param string $site_id
   *   The site identifier.
   */
  public function setSiteId($site_id);

  /**
   * Get the source site ID.
   *
   * @return string
   *   A site identifier.
   */
  public function getSiteId();

  /**
   * Set the source ID.
   *
   * @param string $source_id
   *   A source UUID.
   */
  public function setSourceId($source_id);

  /**
   * Get the Source ID.
   *
   * @return string
   *   A source UUID.
   */
  public function getSourceId();

  /**
   * Set the source entity type.
   *
   * @param string $source_type
   *   The source type.
   */
  public function setSourceType($source_type);

  /**
   * Get the source entity type.
   *
   * @return string
   *   A source type.
   */
  public function getSourceType();

  /**
   * Set the source entity type.
   *
   * @param string $source_type
   *   The source type.
   */
  public function setSourceSubtype($source_type);

  /**
   * Get the source entity type.
   *
   * @return string
   *   A source type.
   */
  public function getSourceSubtype();

  /**
   * Set the timestamp.
   *
   * @param string $timestamp
   *   (optional) A timestamp. Defaults to now.
   */
  public function setTimestamp($timestamp = NULL);

  /**
   * Get the timestamp.
   *
   * @return string
   *   A timestamp.
   */
  public function getTimestamp();

  /**
   * Set the queued timestamp.
   *
   * @param string $timestamp
   *   (optional) A timestamp. Defaults to now.
   */
  public function setQueuedTimestamp($timestamp = NULL);

  /**
   * Get the queued timestamp.
   *
   * @return string
   *   A timestamp.
   */
  public function getQueuedTimestamp();

  /**
   * Set the server.
   *
   * @param string $server
   *   The server name.
   */
  public function setServer($server);

  /**
   * Get the server.
   *
   * @return string
   *   The server name.
   */
  public function getServer();

  /**
   * Set a user who requested the event.
   *
   * @param string $uid
   *   A Drupal user UID.
   */
  public function setRequestingUser($uid);

  /**
   * Return the requesting user.
   *
   * @return string
   *   A UID
   */
  public function getRequestingUser();

  /**
   * Set arbitrary data on the event object.
   *
   * Useful for supporting properties for custom implementations.
   *
   * @param string $key
   *   The key for the data property.
   * @param mixed $value
   *   The value of the data property.
   */
  public function setData($key, $value);

  /**
   * Queue an event.
   *
   * This allows event queueing without having to instantiate a separate
   * Queue handler. Useful if you've just created an event and want to queue
   * it directly.
   *
   * The event will be queued to the server and action queue defined on the
   * event.
   */
  public function queue();

  /**
   * Retrieve a custom data property.
   *
   * @param string $key
   *   The key for the data property.
   * @param mixed $default
   *   A default value to use of the data is not available.
   *
   * @return mixed
   *   The result.
   */
  public function getData($key, $default = NULL);

  /**
   * Re-queue this event.
   */
  public function requeue();

  /**
   * Get this items requeue state.
   */
  public function getRequeueState();

  /**
   * Set an error on the Event.
   *
   * @param bool $has_error
   *   If TRUE, the event has an error.
   * @param string $code
   *   (optional) An error code. Defaults to 0.
   * @param string $message
   *   An error message.
   * @param bool $requeue
   *   (optional) Event should be requeued, defaults to TRUE.
   */
  public function setError($has_error, $code = '0', $message = '', $requeue = TRUE);

  /**
   * Get errors for this event.
   *
   * @return bool|array
   *   FALSE, if no errors occured, or an array of errors.
   */
  public function getErrors();
}
