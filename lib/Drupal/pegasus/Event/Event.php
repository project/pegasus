<?php
/**
 * @file
 * Contains a definition for an event object.
 *
 * @license GPL v2 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Chris Skene chris at previousnext dot com dot au
 */

namespace Drupal\pegasus\Event;

/**
 * Defines an event object.
 */
class Event
  implements EventInterface {

  /**
   * The event identifer, usually a UUID.
   *
   * @var string
   */
  protected $id;

  /**
   * The identifier of the source object.
   *
   * This is often a UUID, but can be a machine name or similar.
   *
   * @var string
   */
  protected $sourceId;

  /**
   * Time the event item was created.
   *
   * @var string
   */
  protected $timestamp;

  /**
   * The action the event is processing.
   *
   * @var string
   */
  protected $action;

  /**
   * The state of the source object
   */
  protected $state;

  /**
   * The source type, usually an entity type.
   *
   * @var string
   */
  protected $sourceType;

  /**
   * The source subtype, for example a bundle or vocab name.
   *
   * @var string
   */
  protected $sourceSubtype;

  /**
   * The site the event came from.
   *
   * There's no explicit handling for the currently, as the server key is used
   * by the API for determining origin, however some implementations may track
   * it.
   *
   * @var string
   */
  protected $siteId;

  /**
   * The time the event was queued.
   *
   * @var string
   */
  protected $queuedTimestamp;

  /**
   * The key for the server endpoint client in use.
   *
   * @var string
   */
  protected $server;

  /**
   * UID of the user requesting the event.
   *
   * @var int
   */
  protected $requestingUser;

  /**
   * An array of custom data properties.
   *
   * @var array
   */
  protected $data;

  /**
   * Whether the event needs to be requeued.
   */
  protected $requeue;

  /**
   * Errors
   */
  protected $errors = array();

  /**
   * Constructor.
   *
   * This sets an ID by default, so you will need to overwrite if you have one.
   */
  public function __construct() {
    // Generate a UUID.
    $this->setId();
    // Set a timestamp.
    $this->setTimestamp();
    // Set a default site ID.
    $this->setSiteId(PEGASUS_SOURCE_NONE);
    $this->data = array();
  }

  /**
   * Set the action.
   *
   * @param string $action
   *   The action.
   */
  public function setAction($action) {
    $this->action = $action;
  }

  /**
   * Get the action.
   *
   * @return string
   *   The action.
   */
  public function getAction() {
    return $this->action;
  }

  /**
   * Set the state.
   *
   * @param string $state
   *   The state of the object.
   */
  public function setState($state) {
    $this->state = $state;
  }

  /**
   * Get the state.
   *
   * @return string
   *   The last state of the object.
   */
  public function getState() {
    return $this->state;
  }

  /**
   * Set the id.
   *
   * @param string $id
   *   An identifier, usually a UUID.
   */
  public function setId($id = NULL) {
    if (empty($id)) {
      $id = uuid_generate();
    }
    $this->id = $id;
  }

  /**
   * Get the identifier.
   *
   * @return string
   *   An identifier, usually a UUID.
   */
  public function getId() {
    return $this->id;
  }

  /**
   * Set the source site ID.
   *
   * @param string $site_id
   *   The site identifier.
   */
  public function setSiteId($site_id) {
    $this->siteId = $site_id;
  }

  /**
   * Get the source site ID.
   *
   * @return string
   *   A site identifier.
   */
  public function getSiteId() {
    return $this->siteId;
  }

  /**
   * Set the source ID.
   *
   * @param string $source_id
   *   A source UUID.
   */
  public function setSourceId($source_id) {
    $this->sourceId = $source_id;
  }

  /**
   * Get the Source ID.
   *
   * @return string
   *   A source UUID.
   */
  public function getSourceId() {
    return $this->sourceId;
  }

  /**
   * Set the source entity type.
   *
   * @param string $source_type
   *   The source type.
   */
  public function setSourceType($source_type) {
    $this->sourceType = $source_type;
  }

  /**
   * Get the source entity type.
   *
   * @return string
   *   A source type.
   */
  public function getSourceType() {
    return $this->sourceType;
  }

  /**
   * Set the source sub type.
   *
   * @param string $subtype
   *   A subtype or bundle.
   */
  public function setSourceSubtype($subtype) {
    $this->sourceSubtype = $subtype;
  }

  /**
   * Get the source sub type.
   *
   * @return string
   *   The subtype.
   */
  public function getSourceSubType() {
    if (isset($this->sourceSubtype)) {
      return $this->sourceSubtype;
    }

    return $this->sourceType;
  }

  /**
   * Set the timestamp.
   *
   * @param string $timestamp
   *   A timestamp.
   */
  public function setTimestamp($timestamp = NULL) {
    if (empty($timestamp)) {
      $timestamp = time();
    }
    $this->timestamp = $timestamp;
  }

  /**
   * Get the timestamp.
   *
   * @return string
   *   A timestamp.
   */
  public function getTimestamp() {
    return $this->timestamp;
  }

  /**
   * Set the queued timestamp.
   *
   * @param string $timestamp
   *   A timestamp.
   */
  public function setQueuedTimestamp($timestamp = NULL) {
    if (empty($timestamp)) {
      $timestamp = time();
    }
    $this->queuedTimestamp = $timestamp;
  }

  /**
   * Get the queued timestamp.
   *
   * @return string
   *   A timestamp.
   */
  public function getQueuedTimestamp() {
    return $this->queuedTimestamp;
  }

  /**
   * Set the server.
   *
   * @param string $server
   *   The server name.
   */
  public function setServer($server) {
    $this->server = $server;
  }

  /**
   * Get the server.
   *
   * @return string
   *   The server name.
   */
  public function getServer() {
    return $this->server;
  }

  /**
   * Set a user who requested the event.
   *
   * @param string $uid
   *   A Drupal user UID.
   */
  public function setRequestingUser($uid) {
    $this->requestingUser = $uid;
  }

  /**
   * Return the requesting user.
   *
   * @return string
   *   A UID
   */
  public function getRequestingUser() {
    if (!isset($this->requestingUser)) {
      return pegasus_get_default_user($this->server);
    }
    return $this->requestingUser;
  }

  /**
   * Set arbitrary data on the event object.
   *
   * Useful for supporting properties for custom implementations.
   *
   * @param string $key
   *   The key for the data property.
   * @param mixed $value
   *   The value of the data property.
   */
  public function setData($key, $value) {
    $this->data[$key] = $value;
  }

  /**
   * Retrieve a custom data property.
   *
   * @param string $key
   *   The key for the data property.
   * @param mixed $default
   *   A default value to use of the data is not available.
   *
   * @return mixed
   *   The result.
   */
  public function getData($key, $default = NULL) {
    if (isset($this->data[$key])) {
      return $this->data[$key];
    }

    return $default;
  }

  /**
   * Queue an event.
   *
   * This allows event queueing without having to instantiate a separate
   * Queue handler. Useful if you've just created an event and want to queue
   * it directly.
   *
   * The event will be queued to the server and action queue defined on the
   * event.
   *
   * @param bool $delayed
   *   If TRUE, the event will be pushed to the back of the queue.
   *
   * @return bool
   *   TRUE, if the item is queued.
   */
  public function queue($delayed = TRUE) {
    $queue = pegasus_queue_handler($this->getServer(), $this->getAction());
    if (!empty($queue)) {
      $queue->enqueue($this, $delayed);
      $return = TRUE;
    }
    else {
      watchdog('pegasus', 'Could not create queue. Server name or Action was not provided', WATCHDOG_WARNING);
      $return = FALSE;
    }
    unset($queue);
    return $return;
  }

  /**
   * Push this item back onto a queue.
   *
   * @param bool $delayed
   *   If TRUE, the event will be pushed to the back of the queue.
   */
  public function requeue($delayed = FALSE) {
    $this->requeue = TRUE;
    pegasus_log('The request for %id had unmet dependencies, and has been queued for re-import.', array(
      '%id' => $this->getSourceId(),
    ), WATCHDOG_NOTICE, FALSE);
    $this->queue($delayed);
  }

  /**
   * Get this items requeue state.
   */
  public function getRequeueState() {
    if (isset($this->requeue) && $this->requeue == TRUE) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Set an error on the Event.
   *
   * @param bool $has_error
   *   If TRUE, the event has an error.
   * @param string $code
   *   (optional) An error code. Defaults to 0.
   * @param string $message
   *   An error message.
   * @param bool $requeue
   *   (optional) Event should be requeued, defaults to TRUE.
   *
   * @throws \Exception
   */
  public function setError($has_error, $code = '0', $message = '', $requeue = TRUE) {
    if ($has_error == FALSE) {
      return;
    }

    $this->errors[] = array(
      'code' => $code,
      'message' => $message,
    );
    $this->requeue($requeue);

    watchdog('pegasus', 'Error in event %id. Message: %message', array(
      '%id' => $this->getId(),
      '%message' => $message,
    ), WATCHDOG_ERROR);
    throw new \Exception($message, $code);
  }

  /**
   * Get errors for this event.
   *
   * @return bool|array
   *   FALSE, if no errors occured, or an array of errors.
   */
  public function getErrors() {
    if (isset($this->errors) && is_array($this->errors)) {
      return $this->errors;
    }

    return FALSE;
  }

  /**
   * Utility function to dump the entire objects properties.
   *
   * This isn't used anyway, only for debugging.
   *
   * @return array
   *   The serialized object.
   */
  public function dump() {
    return get_object_vars($this);
  }
}
