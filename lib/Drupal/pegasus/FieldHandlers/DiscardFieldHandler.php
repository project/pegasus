<?php
/**
 * @file
 * Contains a default field handler to discard the contents of the field.
 */

namespace Drupal\pegasus\FieldHandlers;

/**
 * A field handler to discard the contents of the field.
 */
class DiscardFieldHandler
  extends PropertyHandler
  implements FieldHandlerInterface {

  /**
   * Convert a value from its generic value to a Drupal value.
   *
   * @param mixed $original_value
   *   The original value.
   * @param object|null $target
   *   (optional) The target object onto which to map fields. Some
   *   implementations may set this by reference.
   * @param object|null $source
   *   (optional) The source object from whence fields have come.
   *
   * @return mixed
   *   The value, as required by Drupal.
   */
  public function convertToDrupal($original_value, $target = NULL, $source = NULL) {
    return NULL;
  }

  /**
   * Convert a value from its Drupal value to a generic value.
   *
   * @param mixed $drupal_value
   *   The value, as provided by Drupal.
   * @param object|null $source
   *   (optional) The source object from whence fields have come.
   *
   * @return mixed
   *   The value, as required by the source.
   */
  public function convertFromDrupal($drupal_value, $source = NULL) {
    return NULL;
  }

}
