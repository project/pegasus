<?php
/**
 * @file
 * Contains a property handler for the user ID.
 */

namespace Drupal\pegasus\FieldHandlers;

/**
 * A default property handler.
 *
 * This handler assumes that the original values are correct, and simply
 * writes them directly to the target by returning the raw value.
 */
class PropertyUid
  extends \Drupal\pegasus\FieldHandlers\PropertyHandler
  implements \Drupal\pegasus\FieldHandlers\FieldHandlerInterface {

  /**
   * Convert a value from its generic value to a Drupal value.
   *
   * @param mixed $original_value
   *   The original value.
   * @param object|null $target
   *   (optional) The target object onto which to map fields. Some
   *   implementations may set this by reference.
   * @param object|null $source
   *   (optional) The source object from whence fields have come.
   *
   * @return mixed
   *   The value, as required by Drupal.
   */
  public function convertToDrupal($original_value, $target = NULL, $source = NULL) {

    // Test if the value is set already...
    if ($this->entityHandler->isNew() == FALSE) {
      if (isset($this->entityHandler->getTargetEntity()->uid)) {
        return $this->entityHandler->getTargetEntity()->uid;
      }
    }

    // Then check if the incoming value is a UUID...
    if (uuid_is_valid($original_value)) {
      // Find the user by uuid.
      $matched_users = entity_uuid_load('user', array($original_value));
      if (!empty($matched_users)) {

        $user = reset($matched_users);
        return $user->uid;
      }
    }

    // If a requesting user is set, use it.
    $requesting_user = $this->event->getRequestingUser();
    if (!empty($requesting_user)) {
      return $requesting_user;
    }

    // Fallback on the default.
    return pegasus_get_default_user($this->serverName);
  }

  /**
   * Convert a value from its Drupal value to a generic value.
   *
   * @param mixed $drupal_value
   *   The value, as provided by Drupal.
   * @param array|null $source
   *   (optional) The source object from whence fields have come.
   *
   * @return mixed
   *   The value, as required by the source.
   */
  public function convertFromDrupal($drupal_value, $source = NULL) {
    $users = entity_get_uuid_by_id('user', array($drupal_value));
    if (!empty($users)) {
      return reset($users);
    }

    return $drupal_value;
  }
}
