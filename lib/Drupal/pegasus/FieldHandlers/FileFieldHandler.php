<?php
/**
 * @file
 * Contains a field handler for File fields.
 */

namespace Drupal\pegasus\FieldHandlers;

/**
 * A handler for File field types.
 */
class FileFieldHandler
  extends FieldHandler
    implements FieldHandlerInterface {

  /**
   * Holds a file entity handler.
   *
   * @var \Drupal\pegasus\Entity\FileEntityHandler
   */
  protected $fileEntityHandler;

  /**
   * Convert a value from its generic value to a Drupal value.
   *
   * @param mixed $original_value
   *   The original value.
   * @param object|null $target
   *   (optional) The target object onto which to map fields. Some
   *   implementations may set this by reference.
   * @param array|null $source
   *   (optional) The source object from whence fields have come.
   *
   * @return mixed
   *   The value, as required by Drupal.
   */
  public function convertToDrupal($original_value, $target = NULL, $source = NULL) {

    $return_files = array();

    if (is_object($original_value)) {
      $original_value = $this->objectToArray($original_value);
    }

    foreach ($original_value as $language => $files) {
      foreach ($files as $fid => $file) {
        if (!empty($file) && isset($file['uuid'])) {
          $file_result = $this->getFileEntity($file);
          if (is_array($file_result) && !empty($file_result)) {
            $return_files[$language][$fid] = $file_result;
          }
        }
      }
    }

    $original = count($original_value);
    $new = count($return_files);
    if ($original != $new) {
      $this->event->requeue(TRUE);
    }

    return $return_files;
  }

  /**
   * Convert a value from its Drupal value to a generic value.
   *
   * Iterate through all attached files and provide a URL and hash of contents.
   *
   * @param mixed $drupal_value
   *   The value, as provided by Drupal.
   * @param array|null $source
   *   (optional) The source object from whence fields have come.
   *
   * @return mixed
   *   The value, as required by the source.
   */
  public function convertFromDrupal($drupal_value, $source = NULL) {
    foreach ($drupal_value as $language => $files) {
      foreach ($files as $delta => $file) {
        if (!isset($file['uri'])) {
          $file = file_load($file['fid']);
          $file = (array) $file;
          $drupal_value[$language][$delta]['uri'] = $file['uri'];
          $drupal_value[$language][$delta]['filename'] = $file['filename'];
        }
        $drupal_value[$language][$delta]['url'] = file_create_url($file['uri']);
        if ($file_contents = file_get_contents($file['uri'])) {
          $hash = md5($file_contents);
          $drupal_value[$language][$delta]['hash'] = $hash;
        }
        else {
          watchdog('pegasus', 'Could not hash the file contents. `' . $file['uri'] . '`', array(), WATCHDOG_ERROR);
        }
      }
    }

    return $drupal_value;
  }

  /**
   * Retrieve a file entity.
   *
   * @param array $file
   *   The file data.
   *
   * @return array|bool
   *   The new file data, or FALSE if none should be set.
   */
  protected function getFileEntity($file) {
    $matched_entity = $this->getFileEntityHandler()->matchEntity($file['uuid']);

    // If no matched entity is found, this is new...
    if (empty($matched_entity)) {
      $request = pegasus_event_create_default();
      $request->setSourceId($file['uuid']);
      $request->setAction('pull');
      $request->setSourceType('file');
      $request->setServer($this->serverName);
      $request->queue();

      pegasus_debug('Unmet dependency for ' . $this->event->getSourceId() . '. File ' . $file['uuid'] . ' has been queued for pull.');

      return FALSE;
    }
    // For updated entities, correct the reference.
    else {
      $new_file = (array) $matched_entity;
      $new_file['display'] = isset($file['display']) ? $file['display'] : '';
      $new_file['description'] = isset($file['description']) ? $file['description'] : '';

      return $new_file;
    }
  }

  /**
   * Retrieve a file where the file is not an entity.
   *
   * This method will check for any existing local files with the same uri or
   * filename. If matched, the hashed contents will be compared to the remote
   * file's hash to determine if a local copy exists. If a local copy does
   * exist we copy that file to save from having to transfer it from the remote
   * site.
   *
   * @deprecated
   *
   * @param array $file
   *   The file data.
   *
   * @return array
   *   The new file data.
   */
  protected function getFileItem($file) {
    $return_file = array();
    $import_remote_file = FALSE;

    // Match existing file locally either by uri or filename.
    if ($existing_file = file_load_multiple(array(), array('uri' => $file['uri']))) {
      // Compare this file.
      $existing_file = reset($existing_file);
    }
    elseif ($existing_file = file_load_multiple(array(), array('filename' => $file['filename']))) {
      // Compare this file.
      $existing_file = reset($existing_file);
    }
    else {
      // Create a new file.
      $import_remote_file = TRUE;
    }

    // If existing local file, compare its hash to the remote file hash.
    if ($existing_file) {
      if (isset($file['hash'])) {
        $existing_file_contents = file_get_contents($existing_file->uri);
        $existing_file_hash = md5($existing_file_contents);
        if ($existing_file_hash == $file['hash']) {

          // Same file exists, copy the local file.
          if (file_prepare_directory(dirname($file['uri']), FILE_CREATE_DIRECTORY)) {
            $new_file = file_copy($existing_file, $file['uri'], FILE_EXISTS_RENAME);
            // Need to set a default for this or "Files" will fail.
            $new_file->filename = $file['filename'];
            $new_file->display = isset($file['display']) ? $file['display'] : 1;
            $return_file = $new_file;
          }

        }
        else {
          // File contents differ, flag to import the new file.
          $import_remote_file = TRUE;
        }
      }
      else {
        watchdog('pegasus', 'Could not compare remote file for import as the the hash was not present in the file data.', array(), WATCHDOG_ERROR);
      }
    }

    // Process creating a new file.
    if ($import_remote_file) {
      // We have to import the remote file.
      if (isset($file['url'])) {
        if ($remote_file = file_get_contents($file['url'])) {

          if (file_prepare_directory(dirname($file['uri']), FILE_CREATE_DIRECTORY)) {
            $new_file = file_save_data($remote_file, $file['uri'], FILE_EXISTS_RENAME);
            // Need to set a default for this or "Files" will fail.
            $new_file->filename = $file['filename'];
            $new_file->display = isset($file['display']) ? $file['display'] : 1;
            $return_file = $new_file;
          }
        }
        else {
          watchdog('pegasus', 'Could not retrieve remote file contents. `' . $file['url'] . '`', array(), WATCHDOG_ERROR);
        }
      }
      else {
        watchdog('pegasus', 'Could not attempt to retrieve the remote file because the URL was not present in the file data. This should be set in the function `pegasus_repository_services_hash_file_attachments()` of pegasus_repository_services module on the remote site.', array(), WATCHDOG_ERROR);
      }
    }

    return $return_file;
  }

  /**
   * Get a \Drupal\pegasus\Entity\FileEntityHandler.
   *
   * @return \Drupal\pegasus\Entity\FileEntityHandler
   *   The handler.
   */
  protected function getFileEntityHandler() {
    if (!isset($this->fileEntityHandler) || get_class($this->fileEntityHandler) != '\Drupal\pegasus\Entity\FileEntityHandler') {
      $this->fileEntityHandler = new \Drupal\pegasus\Entity\FileEntityHandler($this->serverName, 'file', NULL);
    }

    return $this->fileEntityHandler;
  }
}
