<?php
/**
 * @file
 * Contains a field handler for Node Reference type fields.
 */

namespace Drupal\pegasus\FieldHandlers;

/**
 * A field handler for a node reference field.
 *
 * This handler assumes that the original values are correct, and simply
 * writes them directly to the target by returning the raw value.
 */
class NodeReferenceFieldHandler
  extends FieldHandler
  implements FieldHandlerInterface {

  /**
   * Convert a value from its generic value to a Drupal value.
   *
   * @param mixed $original_value
   *   The original value.
   * @param object|null $target
   *   (optional) The target object onto which to map fields. Some
   *   implementations may set this by reference.
   * @param object|null $source
   *   (optional) The source object from whence fields have come.
   *
   * @return mixed
   *   The value, as required by Drupal.
   */
  public function convertToDrupal($original_value, $target = NULL, $source = NULL) {

    $return_nodes = array();

    $clean_value = $this->objectToArray($original_value);
    if (!empty($clean_value)) {

      foreach ($clean_value as $language => $nodes) {
        foreach ($nodes as $delta => $node) {
          $key = key($node);
          $uuid = $node;
          $local_node = reset(entity_uuid_load('node', array($uuid)));

          if (!empty($local_node)) {
            $return_nodes[$language][$delta][$key] = $local_node->nid;
          }
        }
      }
    }

    return $return_nodes;
  }

  /**
   * Convert a value from its Drupal value to a generic value.
   *
   * @param mixed $drupal_value
   *   The value, as provided by Drupal.
   * @param array|null $source
   *   (optional) The source object from whence fields have come.
   *
   * @return mixed
   *   The value, as required by the source.
   */
  public function convertFromDrupal($drupal_value, $source = NULL) {
    foreach ($drupal_value as $language => $nodes) {
      foreach ($nodes as $delta => $node) {
        $key = key($node);
        // Try and get the node's uuid.
        if ($uuid = entity_get_uuid_by_id('node', array($node[$key]))) {
          $drupal_value[$language][$delta]['uuid'] = $uuid[$node[$key]];
        }
      }
    }
    return $drupal_value;
  }

}
