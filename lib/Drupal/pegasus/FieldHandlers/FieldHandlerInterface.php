<?php
/**
 * @file
 * Contains an interface for field handler
 */

namespace Drupal\pegasus\FieldHandlers;

/**
 * Defines an interface for interacting with a field handler.
 *
 * In order for a field handler to be detected by PegasusClient, it must implement
 * this interface.
 * @see \PegasusFieldHandler
 */
interface FieldHandlerInterface {

  /**
   * Convert a value from its generic value to a Drupal value.
   *
   * This should return a value suitable for writing directly to the key in the
   * Entity, for example a text field should contain the full array with all
   * values set (we don't use an entity wrapper by default). However, it is
   * possible to set these values by reference and not return them.
   *
   * @param mixed $original_value
   *   The original value.
   * @param object|null $target
   *   (optional) The target object onto which to map fields. Some
   *   implementations may set this by reference.
   * @param array|null $source
   *   (optional) The source object from whence fields have come.
   *
   * @return mixed
   *   The value, as required by Drupal.
   */
  public function convertToDrupal($original_value, $target = NULL, $source = NULL);

  /**
   * Convert a value from its Drupal value to a generic value.
   *
   * Like its convertToDrupal pair, this should return the exact value as
   * expected by the target, however this can also be written to the object by
   * reference.
   *
   * @param mixed $drupal_value
   *   The value, as provided by Drupal.
   * @param array|null $source
   *   (optional) The source object from whence fields have come.
   *
   * @return mixed
   *   The value, as required by the source.
   */
  public function convertFromDrupal($drupal_value, $source = NULL);
}
