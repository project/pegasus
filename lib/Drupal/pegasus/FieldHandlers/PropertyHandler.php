<?php
/**
 * @file
 * Contains a default pass-through field handler.
 */

namespace Drupal\pegasus\FieldHandlers;

/**
 * A default property handler.
 *
 * This handler assumes that the original values are correct, and simply
 * writes them directly to the target by returning the raw value.
 */
class PropertyHandler
  implements FieldHandlerInterface {

  /**
   * The server this field is set for.
   *
   * Some fields need this to properly run their handlers.
   */
  public $serverName;

  /**
   * The event triggering the field map request.
   *
   * @var \Drupal\pegasus\Event\EventInterface
   */
  public $event;

  /**
   * Field type
   */
  public $fieldType;

  /**
   * The calling Entity Handler.
   *
   * @var \Drupal\pegasus\Entity\EntityHandlerInterface
   */
  protected $entityHandler;

  /**
   * Convert a value from its generic value to a Drupal value.
   *
   * @param mixed $original_value
   *   The original value.
   * @param object|null $target
   *   (optional) The target object onto which to map fields. Some
   *   implementations may set this by reference.
   * @param object|null $source
   *   (optional) The source object from whence fields have come.
   *
   * @return mixed
   *   The value, as required by Drupal.
   */
  public function convertToDrupal($original_value, $target = NULL, $source = NULL) {
    return $original_value;
  }

  /**
   * Convert a value from its Drupal value to a generic value.
   *
   * @param mixed $drupal_value
   *   The value, as provided by Drupal.
   * @param array|null $source
   *   (optional) The source object from whence fields have come.
   *
   * @return mixed
   *   The value, as required by the source.
   */
  public function convertFromDrupal($drupal_value, $source = NULL) {
    return $drupal_value;
  }

  /**
   * Helper to convert a returned object to an array.
   *
   * @param object|array $object
   *   The object to convert.
   *
   * @return array
   *   An array representation of the object.
   */
  protected function objectToArray($object) {
    if (!is_object($object) && !is_array($object)) {
      return $object;
    }
    if (is_object($object)) {
      $object = get_object_vars($object);
    }
    return array_map(array($this, 'objectToArray'), $object);
  }

  /**
   * Set the entity handler.
   *
   * @param \Drupal\pegasus\Entity\EntityHandlerInterface $entity_handler
   *   An entity handler.
   */
  public function setEntityHandler($entity_handler) {
    $this->entityHandler = $entity_handler;
  }

  /**
   * Destructor.
   */
  public function __destruct() {
    unset($this->entityHandler);
  }
}
