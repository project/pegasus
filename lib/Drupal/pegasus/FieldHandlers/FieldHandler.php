<?php
/**
 * @file
 * Contains a default pass-through field handler.
 */

namespace Drupal\pegasus\FieldHandlers;

/**
 * A default property handler.
 *
 * This handler assumes that the original values are correct, and simply
 * writes them directly to the target by returning the raw value.
 */
class FieldHandler
  extends PropertyHandler
  implements \Drupal\pegasus\FieldHandlers\FieldHandlerInterface {

  /**
   * Convert a value from its generic value to a Drupal value.
   *
   * @param mixed $original_value
   *   The original value.
   * @param object|null $target
   *   (optional) The target object onto which to map fields. Some
   *   implementations may set this by reference.
   * @param object|null $source
   *   (optional) The source object from whence fields have come.
   *
   * @return mixed
   *   The value, as required by Drupal.
   */
  public function convertToDrupal($original_value, $target = NULL, $source = NULL) {

    if (is_object($original_value)) {
      $return_value = $this->objectToArray($original_value);
    }
    else {
      $return_value = $original_value;
    }

    return $return_value;

  }

  /**
   * Convert a value from its Drupal value to a generic value.
   *
   * @param mixed $drupal_value
   *   The value, as provided by Drupal.
   * @param array|null $source
   *   (optional) The source object from whence fields have come.
   *
   * @return mixed
   *   The value, as required by the source.
   */
  public function convertFromDrupal($drupal_value, $source = NULL) {
    return $drupal_value;
  }
}
