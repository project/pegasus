<?php
/**
 * @file
 * Contains a field handler for Taxonomy term fields.
 *
 * @author tim dot eisenhuth at previousnext dot com dot au
 * @author chris at previousnext dot com dot au
 */

namespace Drupal\pegasus\FieldHandlers;

/**
 * A handler for Taxonomy term fields.
 */
class TermFieldHandler
  extends FieldHandler
    implements FieldHandlerInterface {

  /**
   * Convert a value from its generic value to a Drupal value.
   *
   * This reverses the pattern in PegasusFieldTextHandler::convertFromDrupal()
   *
   * @param mixed $original_value
   *   The original value.
   * @param object|null $target
   *   (optional) The target object onto which to map fields. Some
   *   implementations may set this by reference.
   * @param object|null $source
   *   (optional) The source object from whence fields have come.
   *
   * @return mixed
   *   The value, as required by Drupal.
   */
  public function convertToDrupal($original_value, $target = NULL, $source = NULL) {

    $return_terms = array();
    $requeue = FALSE;

    foreach ($original_value as $language => $terms) {
      foreach ($terms as $tid => $term) {

        $term = (array) $term;
        // Attempt to load this taxonomy term by its UUID.
        if (isset($term['uuid']) && uuid_is_valid($term['uuid'])) {
          $term_unique = $term['uuid'];
        }
        elseif (uuid_is_valid($term['tid'])) {
          // Terms without UUIDs can't be tracked.
          $term_unique = $term['tid'];
        }
        else {
          watchdog('pegasus', 'No valid UUID found for incoming term');
          continue;
        }

        $matched_terms = entity_uuid_load('taxonomy_term', array($term_unique));
        if (!empty($matched_terms)) {

          $import_term = reset($matched_terms);

          // Check we have the vocab locally first. If not, we don't support
          // import whatever the case. Note that this would suggest that the
          // field structure was out of sync, which might indicate a bigger
          // problem, but we don't want to fail spectacularly as a result.
          $vocab = taxonomy_vocabulary_machine_name_load($import_term->vocabulary_machine_name);
          if (!empty($vocab)) {

            // Term exists, load it and return.
            if (!isset($return_terms[$language])) {
              $return_terms[$language] = array();
            }
            $return_terms[$language][$tid] = $this->objectToArray($import_term);
          }
        }
        else {

          // Term does not exist.
          $request = pegasus_event_create_default();
          $request->setSourceId($term_unique);
          $request->setAction('pull');
          $request->setSourceType('taxonomy_term');
          $request->setServer($this->event->getServer());
          $request->queue();

          $requeue = TRUE;

          drupal_set_message('Unmet term dependency for ' . $this->event->getSourceId() . ': ' . $term_unique . ' has been queued for pull.');
        }
      }
    }

    // Trigger event requeueing, if necessary.
    if ($requeue == TRUE) {
      $this->event->requeue();
    }

    return $return_terms;
  }

  /**
   * Convert a value from its Drupal value to a generic value.
   *
   * This breaks a drupal text field down into an array of values, by unpacking
   * the language wrapper.
   *
   * @param mixed $drupal_value
   *   The value, as provided by Drupal.
   * @param array|null $source
   *   (optional) The source object from whence fields have come.
   *
   * @return mixed
   *   The value, as required by the source.
   */
  public function convertFromDrupal($drupal_value, $source = NULL) {
    // Load full taxonomy objects for all terms.
    foreach ($drupal_value as $language => $terms) {
      foreach ($terms as $index => $term) {
        $term_object = taxonomy_term_load($term['tid']);
        $drupal_value[$language][$index] = (array) $term_object;
      }
    }

    return $drupal_value;
  }

}
