<?php
/**
 * @file
 * Provides a class to manage information about a sync status.
 *
 * @copyright Copyright(c) 2012 Previous Next Pty Ltd
 * @license GPL v2 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Chris Skene chris at previousnext dot com dot au
 */

namespace Drupal\pegasus\Core;

/**
 * A class to manage information about sync status.
 */
class SyncStatus {

  /**
   * Sync profiles
   *
   * @var array
   */
  protected $profiles = array();

  /**
   * The items id.
   *
   * @var string
   */
  protected $id;

  /**
   * Constructor.
   *
   * @param null $id
   *   An identifier.
   */
  public function __construct($id = NULL) {
    if (!empty($id)) {
      $this->setId($id);
    }
  }

  /**
   * Start a new profile.
   *
   * @param string|null $handler
   *   Name of the handler.
   * @param string|null $profile_index
   *   An optional name for the profile.
   *
   * @return null|string
   *   The profile_index of the new profile.
   */
  public function newSyncProfile($handler = NULL, $profile_index = NULL) {
    if (empty($profile_index)) {
      $profile_index = uuid_generate();
    }

    $this->profiles[$profile_index] = array();

    if (!empty($handler)) {
      $this->setProfileHandler($profile_index, $handler);
    }

    return $profile_index;
  }

  /**
   * Get all sync profiles.
   *
   * If this returns FALSE, then there are no sync profiles present.
   *
   * @return array|bool
   *   The sync profiles, or FALSE.
   */
  public function getSyncProfiles() {
    if (!empty($this->profiles)) {
      return $this->profiles;
    }

    return FALSE;
  }

  /**
   * Set the items local ID.
   *
   * @param string $id
   *   A local id.
   */
  public function setId($id) {

    $this->id = filter_xss_admin($id);
  }

  /**
   * Get the items local ID.
   *
   * @return bool|string
   *   An ID.
   */
  public function getId() {
    if (isset($this->id)) {
      return $this->id;
    }

    return FALSE;
  }

  /**
   * Flag an item as unsynchronised.
   */
  public function setUnsynched() {
    $this->profiles = array();
  }

  /**
   * Get the synched value.
   *
   * @return boolean
   *   TRUE if the item is synched.
   */
  public function isSynched() {
    if (!empty($this->profiles)) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Set the items source.
   *
   * @param string $profile_index
   *   The profile to use.
   * @param string $source
   *   A source server.
   */
  public function setProfileSource($profile_index, $source) {

    $this->profiles[$profile_index]['source'] = filter_xss_admin($source);
  }

  /**
   * Get the items source.
   *
   * @param string $profile_index
   *   The profile profile_index to use.
   *
   * @return bool|string
   *   The source server.
   */
  public function getProfileSource($profile_index) {

    if (isset($this->profiles[$profile_index]['source'])) {
      return $this->profiles[$profile_index]['source'];
    }

    return FALSE;
  }

  /**
   * Set the source ID.
   *
   * @param string $profile_index
   *   The profile profile_index to use.
   * @param string $source_id
   *   A source ID.
   */
  public function setProfileSourceId($profile_index, $source_id) {

    $this->profiles[$profile_index]['source_id'] = filter_xss_admin($source_id);
  }

  /**
   * Get the source ID.
   *
   * @param string $profile_index
   *   The profile profile_index to use.
   *
   * @return bool|string
   *   The source ID
   */
  public function getProfileSourceId($profile_index) {

    if (isset($this->profiles[$profile_index]['source_id'])) {
      return $this->profiles[$profile_index]['source_id'];
    }

    return FALSE;
  }

  /**
   * Set the sync type.
   *
   * @param string $profile_index
   *   The profile profile_index to use.
   *
   * @param string $type
   *   A valid sync type constant, e.g. PEGASUS_SYNC_TYPE_PULL
   */
  public function setProfileType($profile_index, $type) {

    $this->profiles[$profile_index]['type'] = filter_xss_admin($type);
  }

  /**
   * Get the sync type.
   *
   * @param string $profile_index
   *   The profile profile_index to use.
   *
   * @return bool|string
   *   A valid sync type constant.
   */
  public function getProfileType($profile_index) {

    if (isset($this->profiles[$profile_index]['type'])) {
      return $this->profiles[$profile_index]['type'];
    }

    return FALSE;
  }

  /**
   * Set the handler for this profile.
   *
   * This should correspond to the implementation handling sync for this item.
   *
   * @param string $profile_index
   *   The profile name.
   * @param string $handler_name
   *   Machine name of the handler. Should be the same as the machine name in
   *   the declaration for hook_pegasus_sync_types()
   */
  public function setProfileHandler($profile_index, $handler_name) {
    $this->profiles[$profile_index]['handler'] = filter_xss_admin($handler_name);
  }

  /**
   * Get the profile handler.
   *
   * @param string $profile_index
   *   The profile name.
   *
   * @return string|bool
   *   The machine name of the handler, or FALSE.
   */
  public function getProfileHandler($profile_index) {
    if (isset($this->profiles[$profile_index]['handler'])) {
      return $this->profiles[$profile_index]['handler'];
    }

    return FALSE;
  }

  /**
   * Merge a sync tracker into this tracker.
   *
   * @param \Drupal\pegasus\Core\SyncStatus $tracker
   *   A sync tracker.
   */
  public function mergeSyncTracker(SyncStatus $tracker) {
    $profiles = $tracker->getSyncProfiles();
    if (!empty($profiles)) {
      foreach ($profiles as $profile_index => $profile) {
        $index = $this->newSyncProfile($profile_index);
        $this->setProfileSource($index, $tracker->getProfileSource($profile_index));
        $this->setProfileSourceId($index, $tracker->getProfileSourceId($profile_index));
        $this->setProfileType($index, $tracker->getProfileType($profile_index));
        $this->setProfileHandler($index, $tracker->getProfileHandler($profile_index));
      }
    }
  }
}
