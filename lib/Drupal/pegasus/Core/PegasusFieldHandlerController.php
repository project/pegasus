<?php
/**
 * @file
 * Provides field translation tool for pegasus module fields.
 *
 * @copyright Copyright(c) 2012 PreviousNext
 * @license GPL v2 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Chris Skene chris at previousnext dot com dot au
 */

namespace Drupal\pegasus\Core;

/**
 * Define a constant for a simple object property (i.e. not a field)
 */
use Drupal\pegasus\Event\EventInterface;

define('PEGASUS_SIMPLE_PROPERTY', 'property');

/**
 * Define a constant for a simple property field mapper.
 */
define('PEGASUS_FIELD_HANDLER_PROPERTY', 'property');

/**
 * Define a constant for when a field has a type mapper
 */
define('PEGASUS_FIELD_HANDLER_TYPE', 'field_type');

/**
 * Define a constant for when a field has a custom mapper
 */
define('PEGASUS_FIELD_HANDLER_CUSTOM', 'custom');

/**
 * Define a constant for when no field map exists.
 */
define('PEGASUS_FIELD_HANDLER_NO_FIELD', 'none');

/**
 * Define a constant for when a field has a "pass-through" mapper.
 *
 * This is used when the value can simply be translated 1:1.
 *
 * @deprecated Properties should use PEGASUS_FIELD_HANDLER_PROPERTY
 */
define('PEGASUS_FIELD_HANDLER_PASSTHROUGH', 'pass');

/**
 * Define a constant for when a field has no mapper.
 *
 * These should be discarded.
 */
define('PEGASUS_FIELD_HANDLER_NONE', FALSE);

/**
 * Provides functionality to map values from remote Pegasus server implementations,
 * to local field values.
 *
 * The controller's main purpose is to collect and load individual field
 * handlers from hooks. The handlers are used by PegasusClient and its
 * implementations to translate fields between Drupal and imported/exported
 * data documents.
 */
class PegasusFieldHandlerController {

  /**
   * The event the field handler will be responding to.
   *
   * @var string
   */
  protected $event;

  /**
   * Name of the server in use
   */
  protected $serverName;

  /**
   * An array of field type translation handlers.
   *
   * @var array
   */
  protected $fieldTypeMap;

  /**
   * An array of field-specific translation handlers
   *
   * @var array
   */
  protected $fieldMap;

  /**
   * Track used property handlers
   *
   * @var array
   */
  protected $usedHandlers = array();

  /**
   * An array of keys to skip.
   *
   * @var array
   */
  protected $skipMap = array();

  /**
   * Constructor function.
   *
   * @param \Drupal\pegasus\Event\EventInterface $event
   *   The machine name of the server in use.
   *
   * @return \Drupal\pegasus\Core\PegasusFieldHandlerController
   *   This object, for chaining.
   */
  public function __construct(EventInterface $event) {
    $this->event = $event;
    $this->serverName = $event->getServer();

    return $this;
  }

  /**
   * Load all field mapping information for field types.
   *
   * This loads both field type translation handlers, and field-specific
   * translation handlers, via hooks. In addition to loading all module
   * provided handler configuration, this will load hooks specified in the
   * '_default' configuration, which is searched if none is found in specific
   * implementations.
   * @see PegasusFieldHandlerController::getFieldHandler()
   *
   * @param string $entity_type
   *   The entity type.
   * @param string|null $bundle
   *   (optional) A bundle for the entity type.
   *
   * @throws \Exception
   *
   * @return \Drupal\pegasus\Core\PegasusFieldHandlerController
   *   This object, for chaining.
   */
  public function getFieldHandlers($entity_type, $bundle = NULL) {

    // If no server name is present, bail early and log an error.
    if (empty($this->serverName)) {
      watchdog('pegasus', 'Tried to get field mapper information, but no server name was provided.', array(), WATCHDOG_WARNING);
      throw new \Exception("Tried to get field mapper information, but no server name was provided");
    }
    else {
      // Firstly, load field type handlers via
      // hook_pegasus_field_type_handlers().
      $this->fieldTypeMap = module_invoke_all('pegasus_field_type_handlers', $this->event->getServer(), $entity_type, $bundle);

      // Secondly, load individual field handlers via
      // HOOK_pegasus_field_mappers().
      $this->fieldMap = module_invoke_all('pegasus_field_handlers', $this->event->getServer(), $entity_type, $bundle);
    }

    return $this;
  }

  /**
   * Determine if a field has a handler set, and if so, what type.
   *
   * Returns one of four constants defining a type of handler. These are checked
   * in order, so the first type found wins (e.g. custom handlers override type
   * or property handlers, which override default handlers).
   *   PEGASUS_FIELD_HANDLER_CUSTOM: The field has a custom handler.
   *   PEGASUS_FIELD_HANDLER_PROPERTY: The value has a 1:1 relationship and can
   *    be simply passed through.
   *   PEGASUS_FIELD_HANDLER_TYPE: The field uses a field type handler (e.g. 'text')
   *   PEGASUS_FIELD_HANDLER_TYPE_DEFAULT: Uses a default field handler.
   *   PEGASUS_FIELD_HANDLER_NONE: No field handler could be found.
   *
   * @param string $type
   *   (optional) The type of field.
   * @param string|null $key
   *   (optional) The key of the field.
   *
   * @throws \Exception
   *
   * @return string|bool
   *   A value indicating whether the the field has a mapper, can be passed
   *   through directly, or FALSE.
   */
  public function hasHandler($type = PEGASUS_SIMPLE_PROPERTY, $key = NULL) {
    $handler_type = PEGASUS_FIELD_HANDLER_NONE;
    $handler_name = '\Drupal\pegasus\FieldHandlers\DiscardFieldHandler';

    // Check if the field has a custom handler first.
    if (!empty($key) && array_key_exists($key, $this->fieldMap)) {
      $handler_name = $this->fieldMap[$key];
      $handler_type = PEGASUS_FIELD_HANDLER_CUSTOM;
    }

    // Simple properties can be passed through directly.
    elseif ($type == PEGASUS_SIMPLE_PROPERTY) {
      $handler_name = '\Drupal\pegasus\FieldHandlers\PropertyHandler';
      $handler_type = PEGASUS_FIELD_HANDLER_PROPERTY;
    }

    // Check if the field has a type handler set keyed to the machine name.
    elseif (array_key_exists($type, $this->fieldTypeMap)) {
      $handler_name = $this->fieldTypeMap[$type];
      $handler_type = PEGASUS_FIELD_HANDLER_TYPE;
    }

    // Store this for debugging.
    $this->usedHandlers[$key] = array(
      '#type' => $handler_type,
      '#handler' => $handler_name,
    );

    return $handler_type;
  }

  /**
   * Load a field handler for a given field type and key.
   *
   * Selection of the field handler is based on the following order, taking the
   * first one found:
   *   - The field has a custom, server-provided handler.
   *   - The field is a property, and can be mapped 1:1.
   *   - The field uses a server-provided field type handler.
   *   - The field uses a default field type handler.
   *   - The field has no handler, and is discarded.
   *
   * The return value will (optimally) be an object which implements
   * \FieldHandlerInterface, however where this is not the case, PegasusClient
   * will attempt to load a proxy field handler, as defined by the implementing
   * server, which does implement the interface and can translate that to the
   * field in question.
   * @see PegasusFieldHandlerProxyExample.php
   *
   * @param string $type
   *   The type of field.
   * @param string|null $key
   *   The key of the field.
   *
   * @return object|bool
   *   An object capable of translating fields (e.g. one that implements
   *   \FieldHandlerInterface), or FALSE if none could be loaded.
   */
  public function getFieldHandler($type, $key) {

    // Handlers should be loaded already.
    if (isset($this->usedHandlers[$key])) {
      $handler_info = $this->usedHandlers[$key];
    }
    else {
      return FALSE;
    }

    // Attempt to load the handler object. Calling the object from a function
    // is also supported.
    if (!empty($handler_info['#handler'])) {
      if (class_exists($handler_info['#handler'])) {
        $handler = new $handler_info['#handler']();
      }
      elseif (function_exists($handler_info['#handler'])) {
        $handler = call_user_func($handler_info['#handler']);
      }
    }

    if (empty($handler)) {
      watchdog('pegasus', "Could not load field handler of type %handler. An invalid class was given", array(
        '%handler' => $handler_info['#handler'],
      ), WATCHDOG_ERROR);

      return FALSE;
    }

    $handler->serverName = $this->event->getServer();
    $handler->event = $this->event;
    $handler->fieldType = $type;

    return $handler;
  }

  /**
   * Explicitly set a field handler for a given key.
   *
   * @param string $field_key
   *   The field key to use.
   * @param string $handler_class
   *   The class of the handler. Must be a valid field handler class name.
   */
  public function setFieldHandler($field_key, $handler_class) {
    $this->fieldMap[$field_key] = $handler_class;
  }

  /**
   * Explicitly set a field handler for a given field type.
   *
   * @param string $field_type
   *   The field key to use.
   * @param string $handler_class
   *   The class of the handler. Must be a valid field handler class name.
   */
  public function setFieldTypeHandler($field_type, $handler_class) {
    $this->fieldTypeMap[$field_type] = $handler_class;
  }

  /**
   * Set a property to skip writing.
   *
   * @param string $key
   *   The property name.
   */
  public function skipProperty($key) {
    $this->skipMap[$key] = TRUE;
  }

  /**
   * Test is if a key should be written.
   *
   * @param string $key
   *   The key to test.
   *
   * @return bool
   *   TRUE, if the key is writable. FALSE indicates, usually, that the key has
   *   been set to skip.
   */
  public function isWritable($key) {
    if (array_key_exists($key, $this->skipMap) && $this->skipMap[$key] == TRUE) {
      return FALSE;
    }

    return TRUE;
  }

  /**
   *  Debugging function.
   */
  public function dump() {
    $data = array(
      'field_type' => $this->fieldTypeMap,
      'field' => $this->fieldMap,
      'used_handlers' => $this->usedHandlers,
      'skip_map' => $this->skipMap,
    );
    return $data;
  }
}
