<?php
/**
 * @file
 * The default pegasus client object.
 *
 * @license GPL v2 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Chris Skene chris at previousnext dot com dot au
 */

namespace Drupal\pegasus\Core;

use Drupal\pegasus\Event\EventInterface;
use Drupal\pegasus\Exception\TerminateProcess;

/**
 * A basic class for handling requests to a remote server.
 *
 * This class won't do anything on its own and must be extended.
 */
class PegasusClient
  implements PegasusInterface {

  /**
   * The settings for this client.
   *
   * @var array
   */
  public $settings;

  /**
   * The machine name of this client.
   *
   * @var string
   */
  public $name;

  /**
   * The key used for this client's primary identifier on source documents.
   *
   * @var string
   */
  public $sourcePrimaryId;

  /**
   * Pointer for tracking debugging mode.
   *
   * @var
   */
  protected $debug;

  /**
   * An entity handler for entity-specific operations.
   *
   * @var \Drupal\pegasus\Entity\EntityHandlerInterface
   */
  protected $entityHandler;

  /**
   * Constructor for a PegasusClient.
   *
   * @param string $name
   *   The name of the server.
   * @param array $settings
   *   (optional) An array of settings information.
   */
  public function __construct($name = 'client', $settings = array()) {
    $this->init($name, $settings);
  }

  /**
   * Initialise.
   */
  protected function init($name, $settings) {
    $this->name = $name;
    $this->settings = $settings;

    // Get a key for use as the primary key for the source document.
    // @todo: refactor this so only the method is used normally, and its
    // properly defined.
    if (method_exists($this, 'sourcePrimaryId')) {
      $this->sourcePrimaryId = $this->sourcePrimaryId();
    }
    else {
      $this->sourcePrimaryId = 'id';
    }

    // Turn on debugging, if enabled.
    if (pegasus_debug_is_on()) {
      $this->debugOn();
      pegasus_debug('Pegasus debugging enabled. To disable, visit <a href="/admin/config/services/pegasus/options">options</a>.', 'status', FALSE);
    }
  }

  /** ACTION/EVENT HANDLING METHODS *****************************************/

  /**
   * Retrieve a list of actions this client supports.
   *
   * For a common use, this might be 'pull', 'push' and 'delete', however
   * there may be other types of action as well. PegasusClient defines its own
   * actions, however this is easily overridden in implementations.
   *
   * From this list of actions, the PegasusClient class generates Queue information,
   * as well as providing this information to other modules like messaging, and
   * so on...
   *
   * Actions may also specify a 'method' key, which give a corresponding
   * function name on the handling PegasusClient implementation. This allows you to
   * use function names which are different to your action names.
   *
   * @return array
   *   An array of action types.
   */
  public function getActions() {

    // Event pull action.
    $actions['events'] = array(
      'queue_timeout' => 60,
      'title' => t('Event'),
      'method' => 'getEvents',
      'queue' => 'events',
      'class' => '\Drupal\pegasus\Queue\EventQueueHandler',
    );

    // Default actions.
    $actions['push'] = array(
      'queue_timeout' => 60,
      'title' => t('Push'),
      'method' => 'push',
      'queue' => 'push',
    );
    $actions['delete'] = array(
      'queue_timeout' => 60,
      'title' => t('Delete'),
      'method' => 'delete',
      'queue' => 'pull',
    );
    $actions['pull'] = array(
      'queue_timeout' => 60,
      'title' => t('Pull'),
      'method' => 'pull',
      'queue' => 'pull',
    );

    return $actions;
  }

  /**
   * Push/pull/delete/update an item.
   *
   * This can be called directly, however it is intended to be called by the
   * queue handler [pegasus_queue_process_event()] when stepping through the queue.
   *
   * @param \Drupal\pegasus\Event\EventInterface $event
   *   The Item passed from the Queue Handler.
   *
   * @return mixed
   *   The result of the request.
   */
  public function processItem(EventInterface $event) {

    // Set the client, if it hasn't already.
    $event->setServer($this->name);

    // Log it.
    pegasus_log('Processed event for @id', array('@id' => $event->getId()), WATCHDOG_NOTICE);

    // Implement state switching.
    $this->alterEvent($event);

    // Pass this off to the request function, which deals with method existence.
    $result = $this->request($event->getAction(), $event);

    // Invoke hook_pegasus_post_process()
    module_invoke_all('pegasus_post_process', $event, $result);

    return $result;
  }

  /**
   * Give the client a chance to alter the action before its processed.
   *
   * @param \Drupal\pegasus\Event\EventInterface $event
   *   An event.
   *
   * @return string
   *   The name of the desired action.
   */
  public function alterEvent(EventInterface $event) {
    // Nothing to do.
  }

  /** ENTITY MANIPULATION ***************************************************/

  /**
   * Given a queue item extract the primary identifier of the referenced object.
   *
   * @param array|object $item
   *   A Queue item
   *
   * @return mixed|bool
   *   An identifier, or FALSE.
   */
  protected function extractId($item) {
    // Event items have a callback we can use.
    if ($item instanceof EventInterface) {
      return $item->getSourceId();
    }

    // Other items may need some massaging...
    if (is_object($item)) {
      $item = (array) $item;
    }
    if (isset($item[$this->sourcePrimaryId()])) {
      return $item[$this->sourcePrimaryId()];
    }

    return FALSE;
  }

  /**
   * Get fields for an object.
   *
   * This is a wrapper around the client implementations listFields method,
   * for Interface compatibility and type hinting.
   *
   * @param string $collection
   *   The collection id as returned by PegasusClientInterface::listCollections().
   * @param string $entity_type
   *   The type of entity to list fields for.
   * @param string $bundle
   *   (optional) The bundle to list fields for.
   *
   * @return array
   *   An array of field information.
   */
  public function fieldList($collection, $entity_type, $bundle = NULL) {
    return array();
  }

  /**
   * Get a list of field definitions for a local entity.
   *
   * @param string $entity_type
   *   The entity type.
   * @param string $subtype
   *   (optional) The subtype or bundle. Defaults to the entity type.
   *
   * @return array
   *   An array of field definitions.
   */
  public function entityFieldList($entity_type, $subtype = NULL) {
    $this->setEntityHandler($entity_type);

    return $this->entityHandler->entityFieldList($subtype);
  }

  /**
   * Given an identifier, find a matching entity locally.
   *
   * @param string $entity_type
   *   The type of entity to list fields for.
   * @param string $id
   *   The identifier of the entity to load. May be the entity ID, or for some
   *   client implementations, a UUID or similar.
   *
   * @return object/bool
   *   An entity, or FALSE
   */
  public function matchEntity($entity_type, $id) {

    $this->setEntityHandler($entity_type);

    $this->entityHandler->matchEntity($id);
  }

  /** REQUEST METHODS *******************************************************/

  /**
   * Main function for calling actions on a content client.
   *
   * Calling a method which does not exist will result in an error.
   *
   * @param string $action
   *   The action to call, e.g. 'pull'.
   * @param mixed $event
   *   The event to pass, usually from a queue somewhere.
   * @param array $options
   *   An array of optional parameters. Options include:
   *   'alter': If TRUE, this function will also call a custom alter hook for
   *      this request, in the format:
   *      'hook_pegasus_'.$client_name.'_'.$action .'_alter'.
   *      E.g. 'hook_pegasus_example_server_pull_alter'. This allows other modules
   *      to respond to the request. Defaults to FALSE.
   *   'request args': A string of additional path arguments to attach to the
   *      request.
   *   'request params': An array of parameters to attach to the request.
   *
   * @return mixed|bool
   *   The result of the operation, or FALSE if no method exists.
   */
  public function request($action, $event, $options = array()) {
    $this->setEntityHandler($event->getSourceType());
    $this->entityHandler()->loadFieldMap($event);

    $defined_actions = $this->getActions();

    // Actions can define optional methods. If one is supplied, switch to the
    // method name now. If not, the method called will be the action name.
    // e.g. 'events' calls the 'getEvents' method.
    if (array_key_exists($action, $defined_actions)) {
      if (isset($defined_actions[$action]['method']) && method_exists($this, $defined_actions[$action]['method'])) {
        $action = $defined_actions[$action]['method'];
      }
    }

    try {
      if (!method_exists($this, $action)) {
        pegasus_log('Invalid method %method used on Pegasus client %client_name', array('%method' => $action, '%client_name' => $this->name));
        $event->setError(TRUE);
        return FALSE;
      }

      // Invokes 'hook_pegasus_request_preprocess'.
      // To terminate processing from this hook, throw a new
      // \Drupal\pegasus\Exception\TerminateProcess exception.
      module_invoke_all('pegasus_request_preprocess', $this, $event, $action, $options);

      $result = $this->{$action}($event, $options);
      if ($result === FALSE) {
        return FALSE;
      }

      // Invoke custom alter hooks.
      if (isset($options['alter']) && $options['alter'] == TRUE) {
        $func = 'pegasus_action_' . $action;
        drupal_alter($func, $result);
      }

      // Some transports (e.g. Guzzle) may return an array instead of an object.
      // Normalise it here.
      $data = (array) $result;
      if (isset($data['entity'])) {
        $source_doc = $data['entity'];
      }
      else {
        $source_doc = $data;
      }

      $source_doc = (object) $source_doc;

      return $source_doc;
    }
    catch (TerminateProcess $t) {
      pegasus_log($t->getMessage(), array(), WATCHDOG_NOTICE);
    }

    return FALSE;
  }

  /**
   * Request the events list from the server.
   *
   * This is a special case of action with some items which always need to be
   * set.
   *
   * @param string $last_id
   *   The last ID requested.
   *
   * @return bool|mixed
   *   Result of the request.
   */
  public function requestEvents($last_id) {
    if (empty($last_id)) {
      $last_id = 1;
    }

    $event = pegasus_event_create_default();
    $event->setAction('events');
    $event->setData('last_id', $last_id);
    $event->setServer($this->getName());

    $events = $this->request('events', $event);

    if (!empty($events)) {
      return $events;
    }

    return FALSE;
  }

  /**
   * Wrapper to import an object.
   *
   * @param \Drupal\pegasus\Event\EventInterface $event
   *   An event object.
   * @param bool $mock
   *   (optional) If TRUE, do not save, only return the value.
   *
   * @return mixed
   *   The result of the import, or FALSE.
   */
  public function import(EventInterface $event, $mock = FALSE) {

    $source_doc = $this->request($event->getAction(), $event, array('alter' => TRUE));

    // Error!
    if (empty($source_doc)) {
      pegasus_log('No entity data returned for document %id from server %server in collection %collection', array(
        '%id' => $event->getSourceId(),
        '%collection' => $event->getSourceType(),
        '%server' => $this->getName(),
      ), WATCHDOG_ERROR);

      return FALSE;
    }

    return $this->saveEntity($event, $event->getSourceType(), $source_doc, $event->getSourceId(), $mock);
  }

  /**
   * Save an entity.
   *
   * This will also check for a local matching entity before updating, and run
   * the mapping process.
   *
   * @param \Drupal\pegasus\Event\EventInterface $event
   *   A pegasus module Event.
   * @param string $entity_type
   *   The entity type.
   * @param object|array $source_entity
   *   The source entity.
   * @param string $source_id
   *   Identifier for the source entity.
   * @param bool $mock
   *   TRUE, to not actually save.
   *
   * @return \Drupal\pegasus\Core\SaveResult
   *   A result object.
   */
  public function saveEntity($event, $entity_type, $source_entity, $source_id, $mock = FALSE) {

    $event->setServer($this->getName());

    $this->setEntityHandler($entity_type);
    $this->entityHandler()->setSource($source_entity);

    // Some callbacks still provide a matched entity. We'd like to do that here,
    // but in the case that it occurs, we should use it.
    $preset_match = $event->getData('matched_entity', NULL);
    if (is_object($preset_match)) {
      $this->entityHandler()->setTargetEntity($preset_match);
    }
    else {
      $this->entityHandler()->matchAndSetTarget($source_id);
    }

    // Just some logging...
    $message = 'Created new';
    if ($this->entityHandler()->isNew() == FALSE) {
      $message = 'Updated';
      pegasus_debug('Found matching ' . $entity_type . ' for ' . $event->getSourceId());
    }

    // Map the object.
    $new_entity = $this->entityHandler()->mapToEntity($event);

    if (!empty($new_entity)) {
      // Only save if we are not mocking the method.
      if ($mock == TRUE) {
        $message = 'Requested but did not import';
        $result = pegasus_save_result_create();
        $result->setSuccess(FALSE);
      }
      else {
        $result = $this->entityHandler()->save($entity_type, $new_entity);
        $entity_uri = entity_uri($entity_type, $new_entity);
        $entity_id_link = l(entity_label($entity_type, $new_entity), $entity_uri['path']);
      }
    }
    else {
      $result = pegasus_save_result_create();
      $result->setSuccess(FALSE);
      $message = 'Failed to save';
      $requeue = TRUE;
    }

    pegasus_log('@message entity of type %type: !id', array(
      '@message' => $message,
      '%type' => $entity_type,
      '!id' => isset($entity_id_link) ? $entity_id_link : $source_id,
    ), WATCHDOG_NOTICE, FALSE, PEGASUS_ALWAYS_LOG);

    if (isset($requeue) && $requeue == TRUE) {
      $event->requeue();
    }

    return $result;
  }

  /**
   * Given a source document, map it to a Drupal entity.
   *
   * This method is no longer used internally, as saveEntity() now covers its
   * functionality, however it can still be called where the callee only wishes
   * to convert the entity, as opposed to converting and saving it.
   *
   * @param string $entity_type
   *   The type of entity to list fields for.
   * @param object|array $source_document
   *   The source document returned from the server.
   * @param \Drupal\pegasus\Event\EventInterface $event
   *   A pegasus module Event.
   * @param object|null $local_entity
   *   (optional) A Drupal entity to overwrite values on.
   *
   * @return mixed
   *   The result of the map operation.
   */
  public function mapToEntity($entity_type, $source_document, EventInterface $event, $local_entity = NULL) {

    $this->setEntityHandler($entity_type);

    // Some events don't have a server set at this point.
    $event->setServer($this->getName());

    $this->entityHandler->setSource($source_document);
    $this->entityHandler->setTargetEntity($local_entity);

    return $this->entityHandler->mapToEntity($event);
  }

  /**
   * Given a Drupal entity, map it to a remote document.
   *
   * @param string $entity_type
   *   The type of entity to list fields for.
   * @param object|array $source_document
   *   Object to take values from.
   * @param \Drupal\pegasus\Event\EventInterface $event
   *   A pegasus module Event.
   *
   * @return mixed
   *   The result of the map operation.
   */
  public function mapFromEntity($entity_type, $source_document, EventInterface $event) {

    $this->setEntityHandler($entity_type);

    // Some events don't have a server set at this point.
    $event->setServer($this->getName());

    $this->entityHandler()->setSource($source_document);

    return $this->entityHandler->mapFromEntity($event);
  }

  /**
   * Give the client an opportunity to change the source before mapping.
   *
   * @param \Drupal\pegasus\Entity\EntityHandlerInterface $entity_handler
   *   An entity handler
   */
  public function mapToEntityPreprocess($entity_handler) {
    // Do nothing by default.
  }

  /**
   * Give the client an opportunity to change the source before mapping.
   *
   * @param \Drupal\pegasus\Entity\EntityHandlerInterface $entity_handler
   *   An entity handler
   */
  public function mapFromEntityPreprocess($entity_handler) {
    // Do nothing by default.
  }

  /**
   * Return the collection for a given entity.
   *
   * This usually corresponds to an entity type, but can be different, in which
   * case the individual implementation should override this method.
   *
   * @param object $entity
   *   An entity wrapper object or normal entity.
   * @param string|null $type
   *   (optional) The entity type.
   *
   * @return string
   *   The collection for the given entity.
   */
  public function getCollection($entity, $type = NULL) {
    if ($entity instanceof \EntityDrupalWrapper) {
      return $entity->type();
    }
    else {
      $wrapper = entity_metadata_wrapper($type, $entity);
      return $wrapper->type();
    }

  }

  /** ENTITY HANDLING METHODS ***********************************************/

  /**
   * Set the entity handler.
   *
   * @param string $type
   *   The type of entity.
   * @param string|null $bundle
   *   (optional) a bundle.
   * @param bool $reset
   *   (optional) if TRUE, reload the entity handler.
   */
  public function setEntityHandler($type, $bundle = NULL, $reset = FALSE) {
    if (empty($this->entityHandler) || $reset == TRUE) {
      $handlers = pegasus_entity_handler_info();
      if (array_key_exists($type, $handlers)) {
        if (class_exists($handlers[$type]['class'])) {
          $this->entityHandler = new $handlers[$type]['class']($this->getName(), $type, $bundle);
        }
      }
      else {
        $this->entityHandler = new $handlers['default']['class']($this->getName(), $type, $bundle);
      }

      $this->entityHandler->setClient($this);
    }
  }

  /**
   * Return the active Entity Handler.
   *
   * @return bool|\Drupal\pegasus\Entity\EntityHandlerInterface
   *   An entity handler, or FALSE.
   */
  public function entityHandler() {
    if (isset($this->entityHandler)) {
      return $this->entityHandler;
    }

    return FALSE;
  }

  /**
   * Extract the bundle from a given source.
   *
   * This uses entity bundles by default, however it can be overridden if your
   * source uses a different mechanism to supply this.
   *
   * @param mixed $source
   *   Source document from a content pegasuswork
   * @param null|string $entity_type
   *   (optional) An optional entity type to assist in deriving a bundle.
   *
   * @return mixed
   *   The bundle for the given entity or FALSE if none.
   */
  public function sourceToBundle($source, $entity_type = NULL) {
    $this->setEntityHandler($entity_type);
    return $this->entityHandler->getBundle($source);
  }

  /** QUEUE FUNCTIONS  ******************************************************/

  /**
   * Get the queues required by this source.
   *
   * Queues are created from actions defined in PegasusClient::getActions().
   *
   * @return array
   *   array of queue definitions as per hook_cron_queue_info().
   */
  public function getQueues() {
    $queues = array();

    // General queues are generated for each provided action.
    $actions = $this->getActions();
    if (!empty($actions)) {
      foreach ($actions as $action_name => $action) {

        // Load the queue handler.
        $queue = pegasus_queue_handler_create($action);
        $queue->setServer($this);
        $queue_name = $queue->makeQueueName($action_name);

        // @todo: move this logic into the queue handler
        if (!isset($queues[$action['queue']])) {
          $new_queue = array(
            'time' => $action['queue_timeout'],
            'title' => $this->getName() . ': ' . $action['title'] . ' ' . t('queue'),
            'action' => $action_name,
          );
          isset($action['worker callback']) ? $new_queue['worker callback'] = $action['worker callback'] : $new_queue['worker callback'] = 'pegasus_cron_queue_process_event';

          $queues[$queue_name] = $new_queue;
        }
      }
    }

    return $queues;
  }

  /**
   * Load a QueueHandler.
   *
   * @param array|string $action
   *   (optional) An action configuration from getActions(), or an action name.
   *
   * @return \Drupal\pegasus\Queue\QueueInterface
   *   A Queue Handler.
   */
  public function getQueueHandler($action) {
    $actions = $this->getActions();
    if (isset($actions[$action])) {
      $queue_handler = pegasus_queue_handler_create($actions[$action]);
      $queue_handler->setServer($this);
      $queue_handler->loadQueue($action);

      return $queue_handler;
    }

    return FALSE;
  }

  /** COLLECTION FUNCTIONALITY **********************************************/

  /**
   * Return a list of available collections on the client.
   *
   * @return array
   *   An array of collection names keyed by id.
   */
  public function collectionList() {
    // Nothing to do here.
    return array();
  }

  /** UTILITY FUNCTIONS *****************************************************/

  /**
   * Generate a unique key for a client action.
   *
   * @param string $action
   *   The action to generate a name for.
   *
   * @return string
   *   The name of the action.
   */
  public function actionName($action) {
    return $this->name . '_' . $action;
  }

  /**
   * Return the client name.
   *
   * @return string
   *   The name of the client.
   */
  public function getName() {
    return $this->name;
  }

  /** DEBUGGING FUNCTIONS ***************************************************/

  /**
   * Toggle debugging on.
   */
  public function debugOn() {
    $this->debug = TRUE;
  }

  /**
   * Toggle debugging off.
   */
  public function debugOff() {
    $this->debug = FALSE;
  }

  /**
   * Test the connection to the content repository server.
   *
   * This MUST be overridden in individual implementations in order to work.
   *
   * @return bool
   *   TRUE on a successful connection, or FALSE.
   */
  public function testConnection() {
    return FALSE;
  }

  /**
   * Helper to log debug messages.
   *
   * @deprecated
   *   Use pegasus_debug() instead.
   *
   * @param string $message
   *   The message.
   * @param string $type
   *   (optional) The type. Use drupal_set_message types.
   * @param bool $repeat
   *   (optional) If FALSE, the message will not be repeated.
   */
  public function logDebug($message, $type = 'status', $repeat = TRUE) {

    pegasus_debug($message, $type, $repeat);
  }

  /**
   * Helper to log error messages.
   *
   * Since this passes straight on to watchdog(), all the values passed should
   * match those expected by that function.
   *
   * @deprecated
   *   Use pegasus_log() instead.
   *
   * @param string $message
   *   The message.
   * @param array $substitutions
   *   (optional) An array of substitutions for the message.
   * @param int|string $type
   *   (optional) The type. Use watchdog() constants. Defaults to
   *   WATCHDOG_ERROR.
   */
  public function log($message, $substitutions = array(), $type = WATCHDOG_ERROR) {

    pegasus_log($message, $substitutions, $type);
  }
}
