<?php
/**
 * @file
 * Contains the PegasusClientInterface definition.
 *
 * @license GPL v2 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Chris Skene chris at previousnext dot com dot au
 */

namespace Drupal\pegasus\Core;

/**
 * Defines common functionality required to be implemented on a content server
 * object.
 */
interface PegasusClientInterface
  extends PegasusInterface {

  /**
   * Get a field list for an item type.
   *
   * @param string $collection
   *   The collection id as returned by PegasusClientInterface::listCollections().
   * @param string $entity_type
   *   The entity type to list.
   * @param string|null $bundle
   *   (optional) The bundle type.
   *
   * @return array
   *   An array of field information.
   */
  public function fieldList($collection, $entity_type, $bundle = NULL);

  /**
   * Return the key specifying the source's primary identifier.
   *
   * @return string
   *   A string representing the primary identifier.
   */
  public function sourcePrimaryId();

  /**
   * Return a list of available collections on the server.
   *
   * @return array
   *   An array of collection names keyed by id.
   */
  public function collectionList();

  /**
   * Give the client an opportunity to change the source before mapping.
   *
   * @param \Drupal\pegasus\Entity\EntityHandlerInterface $entity_handler
   *   An entity handler
   */
  public function mapToEntityPreprocess($entity_handler);

  /**
   * Give the client an opportunity to change the source before mapping.
   *
   * @param \Drupal\pegasus\Entity\EntityHandlerInterface $entity_handler
   *   An entity handler
   */
  public function mapFromEntityPreprocess($entity_handler);
}
