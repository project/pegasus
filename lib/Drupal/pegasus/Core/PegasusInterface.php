<?php
/**
 * @file
 * Defines an interface for interacting with a content repository.
 *
 * @license GPL v2 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Chris Skene chris at previousnext dot com dot au
 */

namespace Drupal\pegasus\Core;

use Drupal\pegasus\Event\EventInterface;
use Drupal\pegasus\Interfaces\TestingInterface;

/**
 * Defines an interface for a content repository.
 *
 * This is just a nice wrapper to capture the other interface files, which are
 * separated for readability.
 */
interface PegasusInterface
  extends TestingInterface {

  /**
   * Main function for calling actions on a content server.
   *
   * Calling a method which does not exist will result in an error.
   *
   * @param string $method
   *   The method to call, e.g. 'pull'.
   * @param mixed $event
   *   The event to pass, usually from a queue somewhere.
   * @param array $options
   *   An array of optional parameters. Options include:
   *   'alter': If TRUE, this function will also call a custom alter hook for
   *      this request, in the format:
   *      'hook_pegasus_'.$server_name.'_'.$action .'_alter'.
   *      E.g. 'hook_pegasus_example_server_pull_alter'. This allows other modules
   *      to respond to the request. Defaults to FALSE.
   *   'request args': A string of additional path arguments to attach to the
   *      request.
   *   'request params': An array of parameters to attach to the request.
   *
   * @return mixed|bool
   *   The result of the operation, or FALSE if no method exists.
   */
  public function request($method, $event, $options = array());

  /**
   * Request the events list from the server.
   *
   * This is a special case of action with some items which always need to be
   * set.
   *
   * @param string $last_id
   *   The last ID requested.
   *
   * @return bool|mixed
   *   Result of the request.
   */
  public function requestEvents($last_id);

  /**
   * Retrieve a list of actions this client supports.
   *
   * For a common use, this might be 'pull', 'push' and 'delete', however
   * there may be other types of action as well. PegasusClient defines its own
   * actions, however this is easily overridden in implementations.
   *
   * From this list of actions, the PegasusClient class generates Queue information,
   * as well as providing this information to other modules like messaging, and
   * so on...
   *
   * @return array
   *   An array of action types.
   */
  public function getActions();

  /**
   * Given an identifier, find a matching entity locally.
   *
   * @param string $entity_type
   *   The entity type to look for.
   * @param string $id
   *   The identifier of the entity.
   *
   * @return object/bool
   *   An entity, or FALSE.
   */
  public function matchEntity($entity_type, $id);

  /**
   * Given a source document, map it to a Drupal entity.
   *
   * @param string $entity_type
   *   Th entity type to map for.
   * @param object|array $source_document
   *   The source document.
   * @param \Drupal\pegasus\Event\EventInterface $event
   *   A pegasus module Event.
   * @param object|null $local_entity
   *   (optional) An entity to map to.
   *
   * @return mixed
   *   The result of the map operation.
   */
  public function mapToEntity($entity_type, $source_document, EventInterface $event, $local_entity = NULL);

  /**
   * Wrapper to import an object.
   *
   * @param \Drupal\pegasus\Event\EventInterface $event
   *   An event object.
   * @param bool $mock
   *   (optional) If TRUE, do not save, only return the value.
   *
   * @return mixed
   *   The result of the import, or FALSE.
   */
  public function import(EventInterface $event, $mock = FALSE);

  /**
   * Save an entity.
   *
   * This will also check for a local matching entity before updating, and run
   * the mapping process.
   *
   * @param \Drupal\pegasus\Event\EventInterface $event
   *   A pegasus module Event.
   * @param string $entity_type
   *   The entity type.
   * @param object|array $source_entity
   *   The source entity.
   * @param string $source_id
   *   Identifier for the source entity.
   */
  public function saveEntity($event, $entity_type, $source_entity, $source_id);

  /**
   * Push/pull/delete/update an item.
   *
   * This can be called directly, however it is intended to be called by the
   * queue handler [pegasus_queue_process_event()] when stepping through the queue.
   *
   * @param \Drupal\pegasus\Event\EventInterface $item
   *   The Item passed from the Queue Handler.
   *
   * @return mixed
   *   The result of the request.
   */
  public function processItem(EventInterface $item);

  /**
   * Get information about the available Queues for this server.
   *
   * @return array
   *   An array of queue information.
   */
  public function getQueues();

  /**
   * Load a QueueHandler.
   *
   * @param array|string $action
   *   (optional) An action configuration from getActions(), or an action name.
   *
   * @return \Drupal\pegasus\Queue\QueueInterface
   *   A Queue Handler.
   */
  public function getQueueHandler($action);

  /**
   * Give the server a chance to alter the action before its processed.
   *
   * @param \Drupal\pegasus\Event\EventInterface $event
   *   An event.
   *
   * @return \Drupal\pegasus\Event\EventInterface
   *   The name of the desired action.
   */
  public function alterEvent(EventInterface $event);

  /**
   * Return the collection for a given entity.
   *
   * @param object $entity
   *   An entity wrapper object or normal entity.
   * @param string $type
   *   (optional) The entity type.
   *
   * @return string
   *   The collection for the given entity.
   */
  public function getCollection($entity, $type = NULL);

  /**
   * Extract the bundle from a given source.
   *
   * Implementations should handle both returning of the bundle name and
   * un-setting of the bundle property to ensure it is not mapped to a field
   * value or entity property.
   *
   * @param mixed $source
   *   Source document from a content pegasuswork
   * @param null|string $entity_type
   *   (optional) An optional entity type to assist in deriving a bundle.
   *
   * @return mixed
   *   The bundle for the given entity or NULL if none.
   */
  public function sourceToBundle($source, $entity_type = NULL);

  /**
   * Set the entity handler.
   *
   * @param string $type
   *   The type of entity.
   * @param string|null $bundle
   *   (optional) a bundle.
   * @param bool $reset
   *   (optional) if TRUE, reload the entity handler.
   */
  public function setEntityHandler($type, $bundle = NULL, $reset = FALSE);

  /**
   * Return the active Entity Handler.
   *
   * @return bool|\Drupal\pegasus\Entity\EntityHandlerInterface
   *   An entity handler, or FALSE.
   */
  public function entityHandler();

  /**
   * Return the server name.
   *
   * @return string
   *   The name of the server.
   */
  public function getName();

  /**
   * Generate a unique key for a server action.
   *
   * @param string $action
   *   The action to generate a name for.
   */
  public function actionName($action);
}
