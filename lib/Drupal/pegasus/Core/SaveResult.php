<?php
/**
 * @file
 * Provides a class for tracking a save result.
 *
 * @copyright Copyright(c) 2012 Previous Next Pty Ltd
 * @license GPL v2 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Chris Skene chris at previousnext dot com dot au
 */

namespace Drupal\pegasus\Core;

/**
 * Track a save result.
 */
class SaveResult {

  /**
   * @var bool
   *   TRUE if the item is saved.
   */
  protected $success;

  /**
   * @var mixed
   */
  protected $result;

  /**
   * @var string
   */
  protected $resultId;

  /**
   * Set the result value.
   *
   * @param mixed $result
   *   The result of the save.
   */
  public function setResult($result) {

    $this->result = $result;
  }

  /**
   * Get the result value.
   *
   * @return bool
   *   The result of the save.
   */
  public function getResult() {

    return $this->result;
  }

  /**
   * Set the saved ID.
   *
   * @param string $result_id
   *   The result ID.
   */
  public function setResultId($result_id) {

    $this->resultId = $result_id;
  }

  /**
   * Get the result ID.
   *
   * @return string
   *   The result ID.
   */
  public function getResultId() {

    return $this->resultId;
  }

  /**
   * Set the success value.
   *
   * @param bool $success
   *   TRUE if the item is saved.
   */
  public function setSuccess($success) {

    $this->success = $success;
  }

  /**
   * Get the success value.
   *
   * @return bool
   *   TRUE if the item is saved.
   */
  public function getSuccess() {

    return $this->success;
  }


}
