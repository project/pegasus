<?php
/**
 * @file
 * Contains a class for handling entity's.
 *
 * @copyright Copyright(c) 2012 Previous Next Pty Ltd
 * @license GPL v2 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Chris Skene chris at previousnext dot com dot au
 */

namespace Drupal\pegasus\Entity;

use Drupal\pegasus\FieldHandlers\FieldHandlerInterface;

/**
 * A handler for entities.
 *
 * Specific entity implementations will want to override
 * this to provide per entity customisations.
 */
class EntityHandler
  implements EntityHandlerInterface {

  /**
   * Server name
   *
   * @var string
   */
  protected $serverName;

  /**
   * The entity type
   *
   * @var string
   */
  protected $entityType;

  /**
   * Bundle information
   *
   * @var string
   */
  protected $bundle;

  /**
   * A source entity or document.
   *
   * @var array
   */
  protected $sourceDocument;

  /**
   * A target entity.
   *
   * @var object
   */
  protected $targetEntity;

  /**
   * Tracks information about the target entity map.
   *
   * @var \Drupal\pegasus\Core\PegasusFieldHandlerController
   */
  protected $fieldMap;

  /**
   * The client in use.
   *
   * @var \Drupal\pegasus\Core\PegasusClientInterface
   */
  protected $client;

  /**
   * The event in use.
   *
   * @var \Drupal\pegasus\Event\EventInterface
   */
  protected $event;

  /**
   * Flag for a new entity.
   *
   * @var bool
   */
  protected $isNew;

  /**
   * Constructor.
   *
   * @param string $server_name
   *   The server name.
   * @param string $entity_type
   *   The entity type.
   * @param string|null $bundle
   *   (optional) A bundle.
   */
  public function __construct($server_name, $entity_type, $bundle = NULL) {
    $this->serverName = $server_name;
    $this->entityType = $entity_type;
    if (isset($bundle)) {
      $this->bundle = $bundle;
    }
  }

  /** ENTITY INFORMATION METHODS *******************************************/

  /**
   * Get a list of field definitions for a local entity.
   *
   * @param string $subtype
   *   (optional) The subtype or bundle. Defaults to the entity type.
   *
   * @return array
   *   An array of field definitions.
   */
  public function entityFieldList($subtype = NULL) {
    $server_type_fields = array();
    if (empty($subtype)) {
      $subtype = $this->entityType;
    }

    // Load all fields for this entity/bundle combination.
    $entity_fields = field_info_instances($this->entityType, $subtype);

    if (!empty($entity_fields)) {
      foreach ($entity_fields as $key => $field) {
        // If a field is already set (by a previous field) ignore it.
        if (isset($server_type_fields[$key])) {
          continue;
        }

        // Load additional details.
        $server_type_fields[$key] = (!empty($field_infos[$key]) ? $field_infos[$key] : array()) + $entity_fields[$key];
      }
    }

    // Set up variables for altering.
    $field_data = array();
    $field_data['type'] = $this->entityType;
    $field_data['subtype'] = $subtype;
    $field_data['server'] = $this->serverName;
    $field_data['fields'] = $server_type_fields;

    // Allow implementations to alter this if necessary.
    // Invokes 'hook_pegasus_entity_fields_alter'.
    drupal_alter('pegasus_entity_fields', $field_data);

    return $field_data['fields'];
  }

  /**
   * Extract the bundle from a given source or entity.
   *
   * @param mixed $source
   *   Source document from a content pegasuswork
   *
   * @return string|bool
   *   The bundle for the given entity or FALSE if none.
   */
  public function getBundle($source) {
    $source_array = (array) $source;

    $entity_info = entity_get_info($this->entityType);
    if (isset($entity_info['entity keys'])
      && isset($entity_info['entity keys']['bundle'])
      && isset($source_array[$entity_info['entity keys']['bundle']])) {

      return $source_array[$entity_info['entity keys']['bundle']];
    }

    if (isset($source_array['type'])) {
      return $source_array['type'];
    }

    if (!empty($this->entityType)) {
      return $this->entityType;
    }

    return FALSE;
  }

  /** ENTITY MANAGEMENT METHODS *****************************************/

  /**
   * Save the entity.
   *
   * @param string $entity_type
   *   The entity type.
   * @param object $new_entity
   *   The entity object.
   *
   * @return \Drupal\pegasus\Core\SaveResult
   *   A save result.
   */
  public function save($entity_type, $new_entity) {
    $result = pegasus_save_result_create();

    $save_result = entity_save($entity_type, $new_entity);
    if (!empty($save_result)) {
      $result->setResult($save_result);
    }

    $ids = entity_extract_ids($entity_type, $new_entity);
    if (!empty($ids) && isset($ids[0])) {
      $result->setResultId($ids[0]);
      $result->setSuccess(TRUE);
    }
    else {
      $result->setSuccess(FALSE);
    }

    return $result;
  }

  /** ENTITY TRACKING METHODS *******************************************/

  /**
   * Given an identifier, find a matching entity locally.
   *
   * @param string $id
   *   The identifier of the entity to load. May be the entity ID, or for some
   *   client implementations, a UUID or similar.
   *
   * @return object/bool
   *   An entity, or FALSE
   */
  public function matchEntity($id) {

    // Attempt to load via matching UUID first.
    $matched_entity = reset(entity_uuid_load($this->entityType, array($id)));

    // Give sync implementations an opportunity to match an entity, if uuid_load
    // produces no result.
    if (empty($matched_entity)) {
      $implementations = pegasus_sync_type_info();
      if (!empty($implementations)) {
        foreach ($implementations as $name => $implementation) {
          if (isset($implementation['match callback']) && function_exists($implementation['match callback'])) {
            $matched_entity = call_user_func($implementation['match callback'], $this->serverName, $this->entityType, $id);
          }
        }
      }
    }

    return $matched_entity;
  }

  /**
   * Set the target entity via the local matchEntity method.
   *
   * @param string $id
   *   Identifier to match on.
   */
  public function matchAndSetTarget($id) {
    $match = $this->matchEntity($id);

    if (!empty($match)) {
      $this->setTargetEntity($match);
      $this->setIsNew(FALSE);
    }
    else {
      $target = new \stdClass();
      $this->setTargetEntity($target);
      $this->setIsNew(TRUE);
    }
  }

  /** MAPPING & TRANSLATION METHODS *****************************************/

  /**
   * Prepare the target for mapping.
   *
   * This should be overridden on entity types if required.
   */
  protected function prepareMap() {
    // Do nothing by default.
  }

  /**
   * Given a source document, map it to a Drupal entity.
   *
   * @param \Drupal\pegasus\Event\EventInterface $event
   *   A pegasus module Event.
   *
   * @return mixed
   *   The result of the map operation.
   */
  public function mapToEntity(\Drupal\pegasus\Event\EventInterface $event) {
    $this->setEvent($event);

    // Test we have a bundle/subtype set from somewhere.
    // We include some arbitrary type handling in sourceToBundle, but some
    // implementations may wish to customise it.
    $this->bundle = $this->getBundle($this->sourceDocument, $this->entityType);
    if (empty($this->bundle)) {
      pegasus_log('No entity bundle found for incoming document. Aborting import.', array(), WATCHDOG_ERROR);
      return FALSE;
    }

    // Set a bundle property.
    $bundle_key = pegasus_get_entity_bundle_key($this->entityType);
    $this->sourceDocument[$bundle_key] = $this->bundle;
    $event->setSourceSubtype($this->bundle);

    // Entity type switching is not supported.
    $target = $this->getTargetEntity();
    if (!empty($target) && $this->isNew() == FALSE) {
      $local_bundle = $this->getTargetEntity()->{$bundle_key};
      if ($local_bundle != $this->bundle && !empty($this->bundle)) {
        pegasus_log('Tried to update with incorrect entity types (original: %source, new: %result)', array(
          '%source' => $local_bundle,
          '%result' => $this->bundle,
        ), WATCHDOG_ERROR);
        return FALSE;
      }
    }
    else {
      $this->setIsNew(TRUE);
    }

    // Retrieve a list of fields to expect on the target entity type.
    $target_fields = field_info_instances($this->entityType, $this->bundle);

    // FIELD HANDLER-BASED MAPPING
    // Load a controller object for field types translators.
    // This indirectly calls two hooks:
    // hook_pegasus_field_handlers and hook_pegasus_field_type_handlers. Later, we use
    // PegasusFieldHandlerController::getFieldHandler() to load the field handlers
    // themselves, and use them to convert the source data to our
    // Drupal-friendly data.
    $this->loadFieldMap($event);

    // Give this client an opportunity to change the source structure
    // prior to entity creation. This is likely to be used for removing
    // unwanted keys, mapping original values to new keys, etc.
    $this->client->mapToEntityPreprocess($this);

    /*
     * Now that we have a data set we can use, iterate over fields in the target
     * type and set values from the source.
     */

    // Invoke $this->prepareMap().
    // This is used by individual entity types to perform any necessary
    // operations prior to the map commencing.
    $this->prepareMap();

    // Iterate over our source document, translating source values to Drupal
    // values.
    foreach ($this->sourceDocument as $source_key => $source_value) {
      $property_type = $this->getPropertyType($target_fields, $source_key);
      $this->setPropertyValue('convertToDrupal', $source_key, $source_value, $property_type);
    }

    $target = $this->getTargetEntity();
    if (!empty($target)) {
      // Perform additional mapping of entity properties from the source
      // document map.
      // Invokes 'hook_pegasus_map_to_entity'.
      foreach (module_implements('pegasus_map_to_entity') as $module) {
        $function = $module . '_pegasus_map_to_entity';
        if (function_exists($function)) {
          $function($this, $event, $target, $this->sourceDocument);
        }
      }

      return $target;
    }

    // No entity was created, return FALSE.
    return FALSE;
  }

  /**
   * Given a Drupal entity, map it to a remote document.
   *
   * @param \Drupal\pegasus\Event\EventInterface $event
   *   A pegasus module Event.
   *
   * @return mixed
   *   The result of the map operation.
   */
  public function mapFromEntity(\Drupal\pegasus\Event\EventInterface $event) {
    $this->setEvent($event);

    // Test we have a bundle/subtype set from somewhere.
    // We include some arbitrary type handling in sourceToBundle, but some
    // implementations may wish to customise it.
    $this->bundle = $this->getBundle($this->sourceDocument, $this->entityType);
    if (empty($this->bundle)) {
      watchdog('pegasus', 'No entity type found for incoming document. Aborting import.', array(), WATCHDOG_ERROR);
      return FALSE;
    }

    // Set a bundle property.
    $bundle_key = pegasus_get_entity_bundle_key($this->entityType);
    $this->sourceDocument[$bundle_key] = $this->bundle;
    $event->setSourceSubtype($this->bundle);

    // Retrieve a list of fields to expect on the target entity type.
    $target_fields = field_info_instances($this->entityType, $this->bundle);

    // FIELD HANDLER-BASED MAPPING
    // Load a controller object for field types translators.
    // This indirectly calls two hooks:
    // hook_pegasus_field_handlers and hook_pegasus_field_type_handlers. Later, we use
    // PegasusFieldHandlerController::getFieldHandler() to load the field handlers
    // themselves, and use them to convert the source data to our
    // Drupal-friendly data.
    $this->loadFieldMap($event);

    // Give this client an opportunity to change the source structure
    // prior to entity creation. This is likely to be used for removing
    // unwanted keys, mapping original values to new keys, etc.
    $this->client->mapFromEntityPreprocess($this);

    // Iterate over our source document, translating source values to Drupal
    // values.
    foreach ($this->sourceDocument as $source_key => $source_value) {
      $property_type = $this->getPropertyType($target_fields, $source_key);
      $this->setPropertyValue('convertFromDrupal', $source_key, $source_value, $property_type);
    }

    return (array) $this->getTargetEntity();
  }

  /** ENTITY DATA METHODS ***************************************************/

  /**
   * Set a property value on the tracked target entity.
   *
   * Because entityapi can fail on odd data properties, and that's not even
   * necessarily wrong with remote data, we use our own function.
   *
   * @param string $key
   *   The key to set data for.
   * @param mixed $data
   *   The data.
   *
   * @return bool
   *   TRUE, if the operation succeeds, or FALSE.
   */
  public function setValue($key, $data) {
    try {
      if ($this->getMap()->isWritable($key)) {
        $this->setTargetEntityValue($key, $data);

        $this->getMap()->skipProperty($key);
      }
      else {
        pegasus_log('Skipping ignored property %id', array(
          '%id' => $key,
        ), WATCHDOG_NOTICE);
      }
    }
    catch (\Exception $e) {
      pegasus_log('Could not set field : %error.', array(
        '%error' => $e->getMessage(),
      ), WATCHDOG_NOTICE);

      return FALSE;
    }

    return TRUE;
  }

  /**
   * Set a property on an entity.
   *
   * @param string $method
   *   The method to invoke. e.g. 'convertToDrupal' or 'convertFromDrupal'
   * @param string $source_key
   *   The key to use.
   * @param mixed $source_value
   *   The value to use.
   * @param string $type
   *   The property type.
   */
  protected function setPropertyValue($method, $source_key, $source_value, $type) {

    // Using the translation handler, set the value on the wrapper.
    if ($type != PEGASUS_FIELD_HANDLER_NO_FIELD) {
      $handler_type = $this->fieldMap->hasHandler($type, $source_key);
      if ($handler_type) {
        try {

          // Load the handler.
          $handler = $this->fieldMap->getFieldHandler($type, $source_key);
          if (is_object($handler)) {
            $handler->setEntityHandler($this);

            // Handlers implement \FieldHandlerInterface...
            if ($handler instanceof FieldHandlerInterface) {
              if ($this->fieldMap->isWritable($source_key)) {
                // Run the conversion using the field handler...
                $value = $handler->{$method}($source_value, $this->getTargetEntity(), $this->sourceDocument);
                $this->setValue($source_key, $value);
              }
            }
            else {
              // We can't find a field handler method to convert with.
              pegasus_log("Could not map property %field. The %handler handler appears to be missing a method for conversion.", array(
                '%field' => $source_key,
                '%handler' => get_class($handler),
              ));
            }
          }
          else {
            // No field handler exists!
            pegasus_log("Could not load the specified property handler handler for %field.", array('%field' => $source_key));
          }
        }
        catch (\Exception $e) {
          // Some other exception.
          pegasus_log("Could not map property %field: %error. Line %line, File %file.", array(
            '%error' => $e->getMessage(),
            '%line'  => $e->getLine(),
            '%file'  => $e->getFile(),
            '%field' => $source_key,
          ));
        }
      }
      else {
        // No field handler exists!
        pegasus_log("No property handler specified or available for %field. This field was ignored.", array('%field' => $source_key), WATCHDOG_NOTICE);
      }
    }
  }

  /**
   * Get the property type for a property.
   *
   * @param array $target_fields
   *   An array of fields to target.
   * @param string $source_key
   *   The key to use.
   *
   * @return string
   *   A property type.
   */
  protected function getPropertyType($target_fields, $source_key) {
    if (array_key_exists($source_key, $target_fields)) {
      // We are dealing with a field API field.
      $field_info = field_info_field($source_key);
      $type = $field_info['type'];
    }
    else {
      // This is just a simple property (or may not even exist for new
      // entities, but we cant tell).
      $type = PEGASUS_SIMPLE_PROPERTY;
    }

    return $type;
  }

  /**
   * Set a field handler to use for a property.
   *
   * @param string $key
   *   The property name.
   * @param string $handler_class
   *   The handler class to use.
   */
  public function setFieldHandler($key, $handler_class) {
    $this->fieldMap->setFieldHandler($key, $handler_class);
  }

  /** OBJECT PROPERTY METHODS ***********************************************/

  /**
   * Set the client.
   *
   * @param \Drupal\pegasus\Core\PegasusClientInterface $client
   *   The source document.
   */
  public function setClient($client) {
    $this->client = $client;
  }

  /**
   * Set a source document.
   *
   * @param object|array $source
   *   The source document.
   */
  public function setSource($source) {
    $this->sourceDocument = (array) $source;
  }

  /**
   * Get the source document.
   *
   * @return array|bool
   *   The source document.
   */
  public function getSource() {
    if (isset($this->sourceDocument)) {
      return $this->sourceDocument;
    }

    return FALSE;
  }

  /**
   * Set the target entity.
   *
   * @param object $entity
   *   An entity.
   */
  public function setTargetEntity($entity) {
    $this->targetEntity = $entity;
  }

  /**
   * Retrieve the target entity.
   *
   * @param bool $create
   *   If TRUE, and the entity does not exist, initialise it with a new entity.
   *
   * @return object|bool
   *   An entity, or FALSE.
   */
  public function getTargetEntity($create = FALSE) {
    if (isset($this->targetEntity)) {
      return $this->targetEntity;
    }

    if ($create == TRUE) {
      $bundle_key = pegasus_get_entity_bundle_key($this->entityType);
      $this->targetEntity = entity_create($this->entityType, array($bundle_key => $this->bundle));
      $this->setIsNew(TRUE);
      return $this->targetEntity;
    }

    return FALSE;
  }

  /**
   * Set a value on the target entity.
   *
   * In PHP 5.3.14 and below, calling $this->getTargetEntity()->{$key} can cause
   * a segfault, so we need this method to set this value directly.
   *
   * @param string $key
   *   The key.
   * @param string $value
   *   The data.
   */
  protected function setTargetEntityValue($key, $value) {
    if (!isset($this->targetEntity)) {
      $this->getTargetEntity(TRUE);
    }

    if (is_object($value)) {
      $value = $this->objectToArray($value);
    }

    $this->targetEntity->{$key} = $value;
  }

  /**
   * Set the field controller.
   *
   * @param \Drupal\pegasus\Event\EventInterface $event
   *   A pegasus event.
   */
  public function loadFieldMap($event) {
    $this->fieldMap = new \Drupal\pegasus\Core\PegasusFieldHandlerController($event);
    $this->fieldMap->getFieldHandlers($this->entityType, $this->bundle);
  }

  /**
   * Return the field map for use in mapping operations.
   *
   * @return \Drupal\pegasus\Core\PegasusFieldHandlerController
   *   A field mapping controller.
   */
  public function getMap() {
    return $this->fieldMap;
  }

  /**
   * Get the entity type.
   */
  public function getType() {
    return $this->entityType;
  }

  /**
   * Set the event.
   *
   * @param \Drupal\pegasus\Event\EventInterface $event
   *   An event.
   */
  public function setEvent(\Drupal\pegasus\Event\EventInterface $event) {

    $this->event = $event;
  }

  /**
   * Get the event.
   *
   * @return bool|\Drupal\pegasus\Event\EventInterface
   *   The currently set event.
   */
  public function getEvent() {
    if (isset($this->event)) {
      return $this->event;
    }

    return FALSE;
  }

  /**
   * For this entity type, give us and ID.
   *
   * @param object $entity
   *   An entity.
   *
   * @return string
   *   The local entity ID.
   */
  public function getId($entity) {
    $entity_ids = entity_extract_ids($this->entityType, $entity);
    return $entity_ids[0];
  }

  /**
   * Set the isNew property.
   *
   * @param boolean $is_new
   *   TRUE if the entity is new.
   */
  public function setIsNew($is_new) {

    $this->isNew = $is_new;
  }

  /**
   * Check if an entity is new.
   *
   * This always returns TRUE for pushed entities.
   *
   * @return boolean
   *   Whether the entity is new or not.
   */
  public function isNew() {
    if (!isset($this->isNew)) {
      return FALSE;
    }

    return $this->isNew;
  }

  /**
   * Helper to convert a returned object to an array.
   *
   * @param object|array $object
   *   The object to convert.
   *
   * @return array
   *   An array representation of the object.
   */
  protected function objectToArray($object) {
    if (!is_object($object) && !is_array($object)) {
      return $object;
    }
    if (is_object($object)) {
      $object = get_object_vars($object);
    }
    return array_map(array($this, 'objectToArray'), $object);
  }

  /**
   * Destructor.
   *
   * Unset the client to prevent any memory problems.
   */
  public function __destruct() {
    unset($this->client);
  }
}
