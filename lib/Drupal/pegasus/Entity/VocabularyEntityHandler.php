<?php
/**
 * @file
 * Contains a class for handling entity's.
 *
 * @copyright Copyright(c) 2012 Previous Next Pty Ltd
 * @license GPL v2 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Chris Skene chris at previousnext dot com dot au
 */

namespace Drupal\pegasus\Entity;

/**
 * A handler for entities.
 */
class VocabularyEntityHandler
  extends EntityHandler {

  /**
   * Overrides EntityHandler::matchEntity().
   *
   * No UUID support for vocabularies, so we return them by machine name.
   */
  public function matchEntity($id) {
    $matched_entity = taxonomy_vocabulary_machine_name_load($id);
    if (!empty($matched_entity)) {
      return $matched_entity;
    }

    return FALSE;
  }

}
