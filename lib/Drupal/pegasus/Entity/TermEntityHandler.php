<?php
/**
 * @file
 * Contains a class for handling entity's.
 *
 * @copyright Copyright(c) 2012 Previous Next Pty Ltd
 * @license GPL v2 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Chris Skene chris at previousnext dot com dot au
 */

namespace Drupal\pegasus\Entity;

/**
 * A handler for entities.
 */
class TermEntityHandler
  extends \Drupal\pegasus\Entity\EntityHandler {

  /**
   * Overrides parent::prepareMap().
   */
  protected function prepareMap() {

    $this->sourceDocument['vid'] = NULL;
  }

}
