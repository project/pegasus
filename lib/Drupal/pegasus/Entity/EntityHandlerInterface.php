<?php
/**
 * @file
 * Contains an interface for the entity handler.
 *
 * @copyright Copyright(c) 2012 Previous Next Pty Ltd
 * @license GPL v2 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Chris Skene chris at previousnext dot com dot au
 */

namespace Drupal\pegasus\Entity;

/**
 * Interface for the entity handler.
 */
interface EntityHandlerInterface {

  /**
   * Get a list of field definitions for a local entity.
   *
   * @param string $subtype
   *   (optional) The subtype or bundle. Defaults to the entity type.
   *
   * @return array
   *   An array of field definitions.
   */
  public function entityFieldList($subtype = NULL);

  /**
   * Extract the bundle from a given source.
   *
   * @param mixed $source
   *   Source document from a content pegasuswork
   *
   * @return mixed
   *   The bundle for the given entity or FALSE if none.
   */
  public function getBundle($source);

  /**
   * Save the entity.
   *
   * @param string $entity_type
   *   The entity type.
   * @param object $new_entity
   *   The entity object.
   *
   * @return \Drupal\pegasus\Core\SaveResult
   *   A save result.
   */
  public function save($entity_type, $new_entity);

  /**
   * Given an identifier, find a matching entity locally.
   *
   * @param string $id
   *   The identifier of the entity to load. May be the entity ID, or for some
   *   client implementations, a UUID or similar.
   *
   * @return object/bool
   *   An entity, or FALSE
   */
  public function matchEntity($id);

  /**
   * Set the target entity via the local matchEntity method.
   *
   * @param string $id
   *   Identifier to match on.
   */
  public function matchAndSetTarget($id);

  /**
   * Given a source document, map it to a Drupal entity.
   *
   * @param \Drupal\pegasus\Event\EventInterface $event
   *   A pegasus module Event.
   *
   * @return mixed
   *   The result of the map operation.
   */
  public function mapToEntity(\Drupal\pegasus\Event\EventInterface $event);

  /**
   * Given a Drupal entity, map it to a remote document.
   *
   * @param \Drupal\pegasus\Event\EventInterface $event
   *   A pegasus module Event.
   *
   * @return mixed
   *   The result of the map operation.
   */
  public function mapFromEntity(\Drupal\pegasus\Event\EventInterface $event);

  /**
   * Set a property value on the tracked target entity.
   *
   * Because entityapi can fail on odd data properties, and that's not even
   * necessarily wrong with remote data, we use our own function.
   *
   * @param string $key
   *   The key to set data for.
   * @param mixed $data
   *   The data.
   *
   * @return bool
   *   TRUE, if the operation succeeds, or FALSE.
   */
  public function setValue($key, $data);

  /**
   * Set a field handler to use for a property.
   *
   * @param string $key
   *   The property name.
   * @param string $handler_class
   *   The handler class to use.
   */
  public function setFieldHandler($key, $handler_class);

  /**
   * Set the client.
   *
   * @param \Drupal\pegasus\Core\PegasusClientInterface $client
   *   The source document.
   */
  public function setClient($client);

  /**
   * Set a source document.
   *
   * @param object|array $source
   *   The source document.
   */
  public function setSource($source);

  /**
   * Get the source document.
   *
   * @return array|bool
   *   The source document.
   */
  public function getSource();

  /**
   * Set the target entity.
   *
   * @param object $entity
   *   An entity.
   */
  public function setTargetEntity($entity);

  /**
   * Retrieve the target entity.
   *
   * @param bool $create
   *   If TRUE, and the entity does not exist, initialise it with a new entity.
   *
   * @return object|bool
   *   An entity, or FALSE.
   */
  public function getTargetEntity($create = FALSE);

  /**
   * Set the field controller.
   *
   * @param \Drupal\pegasus\Event\EventInterface $event
   *   A pegasus event.
   */
  public function loadFieldMap($event);

  /**
   * Return the field map for use in mapping operations.
   *
   * @return \Drupal\pegasus\Core\PegasusFieldHandlerController
   *   A field mapping controller.
   */
  public function getMap();

  /**
   * Get the entity type.
   */
  public function getType();

  /**
   * Set the event.
   *
   * @param \Drupal\pegasus\Event\EventInterface $event
   *   An event.
   */
  public function setEvent(\Drupal\pegasus\Event\EventInterface $event);

  /**
   * Get the event.
   *
   * @return \Drupal\pegasus\Event\EventInterface
   *   The currently set event.
   */
  public function getEvent();

  /**
   * For this entity type, give us and ID.
   *
   * @param object $entity
   *   An entity.
   *
   * @return string
   *   The local entity ID.
   */
  public function getId($entity);

  /**
   * Set the isNew property.
   *
   * @param boolean $is_new
   *   TRUE if the entity is new.
   */
  public function setIsNew($is_new);

  /**
   * Check if an entity is new.
   *
   * This always returns TRUE for pushed entities.
   *
   * @return boolean
   *   Whether the entity is new or not.
   */
  public function isNew();
}
