<?php
/**
 * @file
 * Contains a class for handling entity's.
 *
 * @copyright Copyright(c) 2012 Previous Next Pty Ltd
 * @license GPL v2 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Chris Skene chris at previousnext dot com dot au
 */

namespace Drupal\pegasus\Entity;

/**
 * A handler for entities.
 */
class FileEntityHandler
  extends \Drupal\pegasus\Entity\EntityHandler {

  /**
   * Prepare the target for mapping.
   *
   * For new entities, we need to retrieve the remote file itself, save it, and
   * update the source document so that it has relevant keys updated. We use a
   * custom method replacing file_save_data() so that we can explicitly set the
   * UUID.
   */
  protected function prepareMap() {

    if ($this->isNew() == TRUE) {
      $this->sourceDocument = $this->setupRemoteFile($this->sourceDocument);
    }
  }

  /**
   * Given a source file reference, configure a local file reference.
   *
   * @param array $source
   *   The data for the file.
   *
   * @return array|bool
   *   The result of the file save operation, or FALSE.
   */
  public function setupRemoteFile($source) {

    if (isset($source['url'])) {
      if ($remote_file = file_get_contents($source['url'])) {

        // @todo: this sometimes creates 'public' file directories!!
        if (file_prepare_directory(dirname($source['uri']), FILE_CREATE_DIRECTORY)) {
          $new_file = $this->fileSaveData($remote_file, $source['filename'], FILE_EXISTS_RENAME);
          foreach ($new_file as $key => $data) {
            $source[$key] = $data;
          }
        }

        return $source;
      }
      else {
        watchdog('pegasus', 'Could not retrieve remote file contents. `' . $source['url'] . '`', array(), WATCHDOG_ERROR);
      }
    }

    watchdog('pegasus', 'Could not attempt to retrieve the remote file because the URL was not present in the file data. This should be set in the function `pegasus_repository_services_hash_file_attachments()` of pegasus_repository_services module on the remote site.', array(), WATCHDOG_ERROR);

    return FALSE;
  }

  /**
   * Saves a file to the specified destination and creates a database entry.
   *
   * This is a copy of \file_save_data which gives us more control over the
   * properties which are set.
   *
   * @param string $data
   *   A string containing the contents of the file.
   * @param string $destination
   *   A string containing the destination URI. This must be a stream wrapper
   *   URI.
   *   If no value is provided, a randomized name will be generated and the file
   *   will be saved using Drupal's default files scheme, usually "public://".
   * @param int $replace
   *   Replace behavior when the destination file already exists:
   *   - FILE_EXISTS_REPLACE - Replace the existing file. If a managed file with
   *       the destination name exists then its database entry will be updated.
   *       If no database entry is found then a new one will be created.
   *   - FILE_EXISTS_RENAME - Append _{incrementing number} until the filename
   *       is unique.
   *   - FILE_EXISTS_ERROR - Do nothing and return FALSE.
   *
   * @return bool|\stdClass
   *   A file object, or FALSE on error.
   */
  public function fileSaveData($data, $destination = NULL, $replace = FILE_EXISTS_RENAME) {

    $destination = file_default_scheme() . '://' . $destination;

    if (!file_valid_uri($destination)) {
      watchdog('file', 'The data could not be saved because the destination %destination is invalid. This may be caused by improper use of file_save_data() or a missing stream wrapper.', array('%destination' => $destination));
      drupal_set_message(t('The data could not be saved, because the destination is invalid. More information is available in the system log.'), 'error');
      return FALSE;
    }

    if ($uri = file_unmanaged_save_data($data, $destination, $replace)) {
      // Create a file object.
      $file = new \stdClass();
      $file->fid = NULL;
      $file->uri = $uri;
      $file->filename = $this->sourceDocument['filename'];
      $file->filemime = file_get_mimetype($file->uri);
      $file->uid      = pegasus_get_default_user($this->serverName);
      $file->status   = FILE_STATUS_PERMANENT;

      // Explicitly set the UUID!
      $file->uuid = $this->sourceDocument['uuid'];

      // If we are replacing an existing file re-use its database record.
      if ($replace == FILE_EXISTS_REPLACE) {
        $existing_files = file_load_multiple(array(), array('uri' => $uri));
        if (count($existing_files)) {
          $existing = reset($existing_files);
          $file->fid = $existing->fid;
          $file->filename = $existing->filename;
        }
      }
      // If we are renaming around an existing file (rather than a directory),
      // use its basename for the filename.
      elseif ($replace == FILE_EXISTS_RENAME && is_file($destination)) {
        $file->filename = drupal_basename($destination);
      }

      return file_save($file);
    }
    return FALSE;
  }
}
