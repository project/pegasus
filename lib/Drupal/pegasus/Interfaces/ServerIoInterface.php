<?php
/**
 * @file
 * Contains the PegasusServerIoInterface definition.
 *
 * @license GPL v2 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Chris Skene chris at previousnext dot com dot au
 */

namespace Drupal\pegasus\Interfaces;

/**
 * Defines an interface for basic push/pull interaction with a content server.
 *
 * This interface defines push, pull and delete methods. It makes no distinction
 * between a create and an update, as it is assumed that the remote source will
 * determine which action to take based on its own index.
 *
 * @param $string
 * Some stuff.
 */
interface ServerIoInterface {

  /**
   * Push an item to the remote server.
   *
   * @param array $item
   *   The Item passed from the Queue Handler
   *
   * @return mixed
   *   The result of the push operation.
   */
  public function push($item);

  /**
   * Pull an item from the server.
   *
   * @param array $item
   *   The Item passed from the Queue Handler
   * @param boolean $return
   *   TRUE to return without saving the entity, FALSE to save entity
   *
   * @return mixed
   *   The result of the pull operation.
   */
  public function pull($item, $return = FALSE);

  /**
   * Tell the remote server to delete an item.
   *
   * @param array $item
   *   The Item passed from the Queue Handler
   *
   * @return mixed
   *   The result of the delete operation.
   */
  public function delete($item);

}
