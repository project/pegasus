<?php
/**
 * @file
 * Contains an interface for testing a PegasusClient
 *
 * @license GPL v2 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Chris Skene chris at previousnext dot com dot au
 */

namespace Drupal\pegasus\Interfaces;

/**
 * Defines functionality for testing a content repository server.
 */
interface TestingInterface {

  /**
   * Test the connection to the content repository server.
   *
   * This MUST be overridden in individual implementations in order to work.
   *
   * @return bool
   *   TRUE on a successful connection, or FALSE.
   */
  public function testConnection();

  /**
   * Turn debugging on.
   */
  public function debugOn();

  /**
   * Turn debugging off.
   */
  public function debugOff();

  /**
   * Helper to log error messages.
   *
   * Since this passes straight on to watchdog(), all the values passed should
   * match those expected by that function.
   *
   * @param string $message
   *   The message.
   * @param array $substitutions
   *   (optional) An array of substitutions for the message.
   * @param int|string $type
   *   (optional) The type. Use watchdog() constants. Defaults to
   *   WATCHDOG_ERROR.
   */
  public function log($message, $substitutions = array(), $type = WATCHDOG_ERROR);

  /**
   * Helper to log debug messages.
   *
   * @param string $message
   *   The message.
   * @param string $type
   *   (optional) The type. Use drupal_set_message types.
   * @param bool $repeat
   *   (optional) If FALSE, the message will not be repeated.
   */
  public function logDebug($message, $type = 'status', $repeat = TRUE);
}
