<?php
/**
 * @file
 * Provides a custom exception for terminating requests.
 *
 * @copyright Copyright(c) 2012 Previous Next Pty Ltd
 * @license GPL v2 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Chris Skene chris at previousnext dot com dot au
 */

namespace Drupal\pegasus\Exception;

/**
 * A custom exception for terminating requests.
 */
class TerminateProcess
  extends \Exception {

  /**
   * Redefine the exception so message isn't optional
   *
   * @param string $message
   *   The message.
   * @param string $code
   *   (optional) The error code.
   * @param \Exception|null $previous
   *   (optional) The previous exception.
   */
  public function __construct($message, $code = 0, \Exception $previous = null) {

    // make sure everything is assigned properly
    parent::__construct($message, $code, $previous);
  }
}
