<?php
/**
 * @file
 * Contains a PegasusException.
 *
 * @copyright Copyright(c) 2014 Previous Next Pty Ltd
 * @license GPL v2 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Chris Skene chris at previousnext dot com dot au
 */

namespace Drupal\pegasus\Exception;


/**
 * Class PegasusException
 *
 * @package Drupal\pegasus\Exception
 */
class PegasusException extends \Exception {

}
