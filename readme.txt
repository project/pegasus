Pegasus
--------------------------------------------------------------------------------

The Pegasus module provides an API for handling CRUD requests on remote
content services.

In order to use it, you must define your own Client, extending
\PegasusClient() and implementing \PegasusClientInterface().